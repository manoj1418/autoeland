package com.autoeland.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.databinding.LayModelBinding;
import com.autoeland.model.Model;
import com.autoeland.utils.ModelListener;

import java.util.List;

public class ModelAdapter extends RecyclerView.Adapter<ModelAdapter.ViewHolder> {
    ModelListener modelListener;
    List<Model> models;

    public ModelAdapter(ModelListener modelListener, List<Model> models) {
        this.modelListener = modelListener;
        this.models = models;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayModelBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.txName.setChecked(false);
        holder.binding.txName.setOnCheckedChangeListener(null);
        holder.binding.txName.setText(models.get(position).getValue());
        holder.binding.edVariant.setVisibility(holder.binding.txName.isChecked() ? View.VISIBLE : View.GONE);
        holder.binding.txName.setOnCheckedChangeListener((buttonView, isChecked) -> {
            holder.binding.edVariant.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            if (isChecked)
                modelListener.insert_model(models.get(position));
            else
                modelListener.delete_model(models.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void search_filter(List<Model> temp_models) {
        models = temp_models;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LayModelBinding binding;

        ViewHolder(@NonNull LayModelBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}