package com.autoeland.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.databinding.LayChatBinding;
import com.autoeland.model.ChatModel;
import com.autoeland.utils.Utils;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    Context ctx;
    List<ChatModel> chatModels;

    public ChatAdapter(Context ctx, List<ChatModel> chatModels) {
        this.ctx = ctx;
        this.chatModels = chatModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayChatBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (Utils.get_user(ctx).equals(chatModels.get(position).getUser_id())) {
            holder.binding.gIncoming.setVisibility(View.GONE);
            holder.binding.gOutgoing.setVisibility(View.VISIBLE);
            holder.binding.txOutgoing.setText(chatModels.get(position).getMessage());
            holder.binding.txOutgoingTime.setText(Utils.get_time(chatModels.get(position).getDate()));
        } else {
            holder.binding.gOutgoing.setVisibility(View.GONE);
            holder.binding.gIncoming.setVisibility(View.VISIBLE);
            holder.binding.txIncoming.setText(chatModels.get(position).getMessage());
            holder.binding.txIncomingTime.setText(Utils.get_time(chatModels.get(position).getDate()));
        }
    }

    @Override
    public int getItemCount() {
        return chatModels.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LayChatBinding binding;

        ViewHolder(@NonNull LayChatBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}