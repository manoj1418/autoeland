package com.autoeland.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.autoeland.model.Tabs;

import java.util.List;

public class TabAdapter extends FragmentPagerAdapter {
    List<Tabs> tabs;

    public TabAdapter(FragmentManager fm, List<Tabs> tabs) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.tabs = tabs;
    }

    @Override
    public int getCount() {
        return tabs.size();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return tabs.get(position).getFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs.get(position).getTitle();
    }
}