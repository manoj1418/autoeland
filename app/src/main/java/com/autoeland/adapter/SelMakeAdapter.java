package com.autoeland.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.databinding.LayMakeBinding;
import com.autoeland.databinding.LaySelMakeBinding;
import com.autoeland.model.Make;
import com.autoeland.utils.MakeListener;
import com.autoeland.utils.Utils;

import java.util.List;

import okhttp3.internal.Util;

public class SelMakeAdapter extends RecyclerView.Adapter<SelMakeAdapter.ViewHolder> {
    MakeListener makeListener;
    List<Make> makes;

    public SelMakeAdapter(MakeListener makeListener, List<Make> makes) {
        this.makeListener = makeListener;
        this.makes = makes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LaySelMakeBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.icImg.setImageResource(makes.get(position).getRes_id());
        holder.binding.txName.setText(makes.get(position).getValue());
        holder.binding.txModel.setText(Utils.get_models(makes.get(position).getModels()));
        holder.binding.btnDelete.setOnClickListener(v -> makeListener.delete_make(makes.get(position)));
    }

    @Override
    public int getItemCount() {
        return makes.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LaySelMakeBinding binding;

        ViewHolder(@NonNull LaySelMakeBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}