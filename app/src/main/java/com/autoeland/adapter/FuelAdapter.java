package com.autoeland.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.databinding.LayFuelBinding;
import com.autoeland.model.ChoiceModel;
import com.autoeland.utils.FuelListener;
import com.autoeland.utils.Utils;

import java.util.List;

public class FuelAdapter extends RecyclerView.Adapter<FuelAdapter.ViewHolder> {
    FuelListener fuelListener;
    List<ChoiceModel> choiceModels;

    public FuelAdapter(FuelListener fuelListener, List<ChoiceModel> choiceModels) {
        this.fuelListener = fuelListener;
        this.choiceModels = choiceModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayFuelBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.txName.setChecked(false);
        holder.binding.txName.setOnCheckedChangeListener(null);
        holder.binding.txName.setChecked(choiceModels.get(position).isIs_checked());
        holder.binding.txName.setText(Utils.check_str(choiceModels.get(position).getName()));
        holder.binding.txName.setOnCheckedChangeListener((buttonView, isChecked) -> fuelListener.select_fuel(choiceModels.get(position), isChecked));
    }

    @Override
    public int getItemCount() {
        return choiceModels.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LayFuelBinding binding;

        ViewHolder(@NonNull LayFuelBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}