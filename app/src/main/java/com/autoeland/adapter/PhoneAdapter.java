package com.autoeland.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.databinding.LayFeaturesBinding;
import com.autoeland.databinding.LayPhonesBinding;
import com.autoeland.model.Phone;
import com.autoeland.utils.PhoneListener;
import com.autoeland.utils.Utils;

import java.util.List;

public class PhoneAdapter extends RecyclerView.Adapter<PhoneAdapter.ViewHolder> {
    PhoneListener phoneListener;
    List<Phone> phones;

    public PhoneAdapter(PhoneListener phoneListener, List<Phone> phones) {
        this.phoneListener = phoneListener;
        this.phones = phones;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayPhonesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.txName.setText(Utils.check_str(phones.get(position).getNumber()));
        holder.binding.txName.setOnClickListener(v -> phoneListener.click_call(phones.get(position).getUri()));
    }

    @Override
    public int getItemCount() {
        return phones.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LayPhonesBinding binding;

        ViewHolder(@NonNull LayPhonesBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}