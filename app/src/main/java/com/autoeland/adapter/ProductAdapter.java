package com.autoeland.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.databinding.LayProductBinding;
import com.autoeland.model.Product;
import com.autoeland.utils.ProductListener;
import com.autoeland.utils.Utils;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    ProductListener productListener;
    List<Product> products;

    public ProductAdapter(ProductListener productListener, List<Product> products) {
        this.productListener = productListener;
        this.products = products;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayProductBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (products.get(position).getImages() != null && products.get(position).getImages().size() > 0)
            Utils.set_image(products.get(position).getImages().get(0).getMedium(), holder.binding.icPic);
        holder.binding.txPrice.setText(Utils.get_currency_symbol(products.get(position).getCurrency(), Utils.get_currency_format(products.get(position).getPrice())));
        holder.binding.txTitle.setText(products.get(position).getTitle());
        holder.binding.layCons.setOnClickListener(v -> productListener.open_product(products.get(position)));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LayProductBinding binding;

        ViewHolder(@NonNull LayProductBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}