package com.autoeland.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.databinding.LayPicBinding;
import com.autoeland.utils.Utils;

import java.util.List;

public class SelPicAdapter extends RecyclerView.Adapter<SelPicAdapter.ViewHolder> {
    List<Uri> uri;

    public SelPicAdapter(List<Uri> uri) {
        this.uri = uri;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayPicBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Utils.set_image(String.valueOf(uri.get(position)), holder.binding.icImg);
    }

    @Override
    public int getItemCount() {
        return uri.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LayPicBinding binding;

        ViewHolder(@NonNull LayPicBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}