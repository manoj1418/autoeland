package com.autoeland.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.databinding.LayConditionBinding;
import com.autoeland.model.ChoiceModel;
import com.autoeland.utils.ConditionListener;
import com.autoeland.utils.Utils;

import java.util.List;

public class ConditionAdapter extends RecyclerView.Adapter<ConditionAdapter.ViewHolder> {
    ConditionListener conditionListener;
    List<ChoiceModel> choiceModels;

    public ConditionAdapter(ConditionListener conditionListener, List<ChoiceModel> choiceModels) {
        this.conditionListener = conditionListener;
        this.choiceModels = choiceModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayConditionBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.txName.setChecked(false);
        holder.binding.txName.setOnCheckedChangeListener(null);
        holder.binding.txName.setChecked(choiceModels.get(position).isIs_checked());
        holder.binding.txName.setText(Utils.check_str(choiceModels.get(position).getName()));
        holder.binding.txName.setOnCheckedChangeListener((buttonView, isChecked) -> conditionListener.select_condition(choiceModels.get(position), isChecked));
    }

    @Override
    public int getItemCount() {
        return choiceModels.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LayConditionBinding binding;

        ViewHolder(@NonNull LayConditionBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}