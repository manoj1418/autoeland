package com.autoeland.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.databinding.LayMakeBinding;
import com.autoeland.model.Make;
import com.autoeland.utils.MakeListener;

import java.util.List;

public class MakeAdapter extends RecyclerView.Adapter<MakeAdapter.ViewHolder> {
    MakeListener makeListener;
    List<Make> makes;

    public MakeAdapter(MakeListener makeListener, List<Make> makes) {
        this.makeListener = makeListener;
        this.makes = makes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayMakeBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.icImg.setImageResource(makes.get(position).getRes_id());
        holder.binding.txName.setText(makes.get(position).getValue());
        holder.binding.layCons.setOnClickListener(v -> makeListener.click_make(makes.get(position)));
    }

    @Override
    public int getItemCount() {
        return makes.size();
    }

    public void search_filter(List<Make> temp_makes) {
        makes = temp_makes;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LayMakeBinding binding;

        ViewHolder(@NonNull LayMakeBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}