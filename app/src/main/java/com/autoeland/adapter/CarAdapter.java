package com.autoeland.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.R;
import com.autoeland.databinding.LayItemListBinding;
import com.autoeland.model.Car;
import com.autoeland.utils.CarListener;
import com.autoeland.utils.Utils;

import java.util.List;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {
    Context ctx;
    List<Car> carList;
    CarListener carListener;

    public CarAdapter(Context ctx, List<Car> carList, CarListener carListener) {
        this.ctx = ctx;
        this.carList = carList;
        this.carListener = carListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayItemListBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Car car = carList.get(position);
        if (car != null) {
            if (car.getImages() != null && car.getImages().size() > 0)
                Utils.set_image(Utils.set_image_url(car.getImages().get(0).getUri()), holder.binding.icImg);
            holder.binding.txName.setText(Utils.check_str(car.getTitle()));
            holder.binding.txPrice.setText(Utils.check_str(car.getP()));
            holder.binding.txDesc.setText(Utils.check_str(car.getDetails().get(1)));
            holder.binding.txTech.setText("");
            holder.binding.txAgency.setText("");
            holder.binding.txLocation.setText(Utils.check_str(car.getDetails() != null && car.getDetails().size() > 0 ? car.getDetails().get(0) : "-" ));
            holder.binding.icParking.setImageResource(Utils.set_parking_icon(ctx, car) ? R.drawable.ic_park_filled : R.drawable.ic_park_outline);
             holder.binding.layCons.setOnClickListener(v -> carListener.click_car(car));
            holder.binding.icCall.setOnClickListener(v -> carListener.click_call(car.getId()));
            holder.binding.icParking.setOnClickListener(v -> carListener.click_park(car, holder.binding.icParking, holder.binding.idProgressParking));
        }
    }

    @Override
    public int getItemCount() {
        return carList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LayItemListBinding binding;

        ViewHolder(@NonNull LayItemListBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }
}