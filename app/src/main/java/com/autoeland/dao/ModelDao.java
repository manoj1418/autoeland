package com.autoeland.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.autoeland.model.Make;
import com.autoeland.model.Model;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ModelDao {
    @Insert(onConflict = REPLACE)
    void insert_model(List<Model> ml);

    @Query("select * from model")
    List<Model> get_all_model();

    @Delete
    void delete_model(Model ml);
}