package com.autoeland.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.autoeland.model.Version;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface VersionDao {
    @Insert(onConflict = REPLACE)
    void insert_version(List<Version> ml);

    @Query("select * from versiones")
    List<Version> get_all_version();

    @Query("select * from versiones where make_i= :brand and model_i= :model")
    List<Version> get_version_from_model(String brand, String model);

    @Delete
    void delete_version(Version ml);
}