package com.autoeland.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.autoeland.model.Make;

import java.util.List;

import io.reactivex.Single;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MakeDao {
    @Insert(onConflict = REPLACE)
    void insert_make(List<Make> mk);

    @Query("select * from make")
    List<Make> get_all_make();

    @Delete
    void delete_make(Make mk);
}