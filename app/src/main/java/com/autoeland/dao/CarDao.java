package com.autoeland.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.autoeland.model.Car;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface CarDao {
    @Insert(onConflict = REPLACE)
    void insert_car(Car car);

    @Query("select * from car")
    List<Car> get_all_car();

    @Query("select count(*) from car where id =:id")
    int is_car_available(int id);

    @Delete
    void delete_car(Car car);
}