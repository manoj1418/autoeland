package com.autoeland.model;

import java.util.Date;

public class ChatModel {
    private String user_id;
    private String message;
    private Date date;

    public ChatModel(String user_id, String message, Date date) {
        this.user_id = user_id;
        this.message = message;
        this.date = date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}