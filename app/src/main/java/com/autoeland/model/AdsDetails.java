package com.autoeland.model;

import java.util.List;

public class AdsDetails {
    private String htmlDescription;
    private List<Financing> financePlans;

    public String getHtmlDescription() {
        return htmlDescription;
    }

    public void setHtmlDescription(String htmlDescription) {
        this.htmlDescription = htmlDescription;
    }

    public List<Financing> getFinancePlans() {
        return financePlans;
    }

    public void setFinancePlans(List<Financing> financePlans) {
        this.financePlans = financePlans;
    }
}