package com.autoeland.model;

import java.util.List;

public class ProductList {
    private List<Product> search_objects;

    public List<Product> getSearch_objects() {
        return search_objects;
    }

    public void setSearch_objects(List<Product> search_objects) {
        this.search_objects = search_objects;
    }
}