package com.autoeland.model;

import java.io.File;

public class ImageUpload {
    private File file;

    public ImageUpload(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
