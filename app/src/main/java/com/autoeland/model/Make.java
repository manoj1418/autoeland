package com.autoeland.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "make")
public class Make {
    @PrimaryKey
    @SerializedName("i")
    @NonNull
    private String key;
    @SerializedName("n")
    private String value;
    private int res_id;
    @Ignore
    private List<Model> models;

    public Make() {
    }

    @Ignore
    public Make(@NonNull String key, String value, int res_id) {
        this.key = key;
        this.value = value;
        this.res_id = res_id;
    }

    @NonNull
    public String getKey() {
        return key;
    }

    public void setKey(@NonNull String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getRes_id() {
        return res_id;
    }

    public void setRes_id(int res_id) {
        this.res_id = res_id;
    }

    public List<Model> getModels() {
        return models;
    }

    public void setModels(List<Model> models) {
        this.models = models;
    }
}