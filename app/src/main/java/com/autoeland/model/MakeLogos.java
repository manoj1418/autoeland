package com.autoeland.model;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.exifinterface.media.ExifInterface;

import com.autoeland.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MakeLogos {
    private List<Make> makes;
    private List<Model> models;
    @NonNull
    private static Map<String, Integer> LOGO_MAP = new HashMap<>();
    @DrawableRes
    public static int MISSING_LOGO = 0;

    private MakeLogos() {
    }

    @NonNull
    public static Map<String, Integer> getLOGO_MAP() {
        if (LOGO_MAP.size() < 115) {
            LOGO_MAP.clear();
            LOGO_MAP.put("140", R.drawable.abarth);
            LOGO_MAP.put("203", R.drawable.ac);
            LOGO_MAP.put("375", R.drawable.acura);
            LOGO_MAP.put("800", R.drawable.aixam);
            LOGO_MAP.put("900", R.drawable.alfaromeo);
            LOGO_MAP.put("1100", R.drawable.alpina);
            LOGO_MAP.put("121", R.drawable.artega);
            LOGO_MAP.put("1750", R.drawable.asia_motors);
            LOGO_MAP.put("1700", R.drawable.aston_martin);
            LOGO_MAP.put("1900", R.drawable.audi);
            LOGO_MAP.put("2000", R.drawable.austin_morris);
            LOGO_MAP.put("1950", R.drawable.austin_healey);
            LOGO_MAP.put("3100", R.drawable.bentley);
            LOGO_MAP.put("3500", R.drawable.bmw);
            LOGO_MAP.put("3850", R.drawable.borgward);
            LOGO_MAP.put("4025", R.drawable.brilliance);
            LOGO_MAP.put("4350", R.drawable.bugatti);
            LOGO_MAP.put("4400", R.drawable.buick);
            LOGO_MAP.put("4700", R.drawable.cadillac);
            LOGO_MAP.put("112", R.drawable.casalini);
            LOGO_MAP.put("5300", R.drawable.caterham);
            LOGO_MAP.put("83", R.drawable.chatenet);
            LOGO_MAP.put("5600", R.drawable.chevrolet);
            LOGO_MAP.put("5700", R.drawable.chrysler);
            LOGO_MAP.put("5900", R.drawable.citroen);
            LOGO_MAP.put("6200", R.drawable.cobra);
            LOGO_MAP.put("6325", R.drawable.corvette);
            LOGO_MAP.put(ExifInterface.GPS_MEASUREMENT_3D, R.drawable.cupra);
            LOGO_MAP.put("6600", R.drawable.dacia);
            LOGO_MAP.put("6800", R.drawable.daewoo);
            LOGO_MAP.put("7000", R.drawable.daihatsu);
            LOGO_MAP.put("7400", R.drawable.de_tomaso);
            LOGO_MAP.put("7700", R.drawable.dodge);
            LOGO_MAP.put("255", R.drawable.donkervoort);
            LOGO_MAP.put("235", R.drawable.citroends);
            LOGO_MAP.put("8600", R.drawable.ferrari);
            LOGO_MAP.put("8800", R.drawable.fiat);
            LOGO_MAP.put("172", R.drawable.fisker);
            LOGO_MAP.put("9000", R.drawable.ford);
            LOGO_MAP.put("205", R.drawable.gac_gonow);
            LOGO_MAP.put("204", R.drawable.gemballa);
            LOGO_MAP.put("9900", R.drawable.gmc);
            LOGO_MAP.put("122", R.drawable.grecav);
            LOGO_MAP.put("186", R.drawable.hamann);
            LOGO_MAP.put("10850", R.drawable.holden);
            LOGO_MAP.put("11000", R.drawable.honda);
            LOGO_MAP.put("11050", R.drawable.hummer);
            LOGO_MAP.put("11600", R.drawable.hyundai);
            LOGO_MAP.put("11650", R.drawable.infiniti);
            LOGO_MAP.put("11900", R.drawable.isuzu);
            LOGO_MAP.put("12100", R.drawable.iveco);
            LOGO_MAP.put("12400", R.drawable.jaguar);
            LOGO_MAP.put("12600", R.drawable.jeep);
            LOGO_MAP.put("13200", R.drawable.kia);
            LOGO_MAP.put("13450", R.drawable.koenigsegg);
            LOGO_MAP.put("13900", R.drawable.ktm);
            LOGO_MAP.put("14400", R.drawable.lada);
            LOGO_MAP.put("14600", R.drawable.lamborghini);
            LOGO_MAP.put("14700", R.drawable.lancia);
            LOGO_MAP.put("14800", R.drawable.landrover);
            LOGO_MAP.put("14845", R.drawable.landwind);
            LOGO_MAP.put("15200", R.drawable.lexus);
            LOGO_MAP.put("15400", R.drawable.ligier);
            LOGO_MAP.put("15500", R.drawable.lincoln);
            LOGO_MAP.put("15900", R.drawable.lotus);
            LOGO_MAP.put("16200", R.drawable.mahindra);
            LOGO_MAP.put("16600", R.drawable.maserati);
            LOGO_MAP.put("16700", R.drawable.maybach);
            LOGO_MAP.put("16800", R.drawable.mazda);
            LOGO_MAP.put("137", R.drawable.mclaren);
            LOGO_MAP.put("17200", R.drawable.mercedes);
            LOGO_MAP.put("17300", R.drawable.mg);
            LOGO_MAP.put("30011", R.drawable.microcar);
            LOGO_MAP.put("17500", R.drawable.mini);
            LOGO_MAP.put("17700", R.drawable.mitsubishi);
            LOGO_MAP.put("17900", R.drawable.morgan);
            LOGO_MAP.put("18700", R.drawable.nissan);
            LOGO_MAP.put("18875", R.drawable.nsu);
            LOGO_MAP.put("18975", R.drawable.oldsmobile);
            LOGO_MAP.put("19000", R.drawable.opel);
            LOGO_MAP.put("149", R.drawable.pagani);
            LOGO_MAP.put("19300", R.drawable.peugeot);
            LOGO_MAP.put("19600", R.drawable.piaggio);
            LOGO_MAP.put("19800", R.drawable.plymouth);
            LOGO_MAP.put("4", R.drawable.polestar);
            LOGO_MAP.put("20000", R.drawable.pontiac);
            LOGO_MAP.put("20100", R.drawable.porsche);
            LOGO_MAP.put("20200", R.drawable.proton);
            LOGO_MAP.put("20700", R.drawable.renault);
            LOGO_MAP.put("21600", R.drawable.rolls_royce);
            LOGO_MAP.put("21700", R.drawable.rover);
            LOGO_MAP.put("125", R.drawable.ruf);
            LOGO_MAP.put("21800", R.drawable.saab_new);
            LOGO_MAP.put("22000", R.drawable.santana);
            LOGO_MAP.put("22500", R.drawable.seat);
            LOGO_MAP.put("22900", R.drawable.skoda);
            LOGO_MAP.put("23000", R.drawable.smart);
            LOGO_MAP.put("188", R.drawable.speedart);
            LOGO_MAP.put("100", R.drawable.spyker);
            LOGO_MAP.put("23100", R.drawable.ssangyong);
            LOGO_MAP.put("23500", R.drawable.subaru);
            LOGO_MAP.put("23600", R.drawable.suzuki);
            LOGO_MAP.put("23800", R.drawable.talbot);
            LOGO_MAP.put("23825", R.drawable.tata);
            LOGO_MAP.put("189", R.drawable.techart);
            LOGO_MAP.put("135", R.drawable.tesla);
            LOGO_MAP.put("24100", R.drawable.toyota);
            LOGO_MAP.put("24200", R.drawable.trabant);
            LOGO_MAP.put("24400", R.drawable.triumph);
            LOGO_MAP.put("24500", R.drawable.tvr);
            LOGO_MAP.put("25200", R.drawable.vw);
            LOGO_MAP.put("25100", R.drawable.volvo);
            LOGO_MAP.put("25300", R.drawable.wartburg);
            LOGO_MAP.put("113", R.drawable.westfield);
            LOGO_MAP.put("25650", R.drawable.wiesmann);
        }
        return LOGO_MAP;
    }

    public static int getLogo(@Nullable Make make) {
        if (make == null) {
            return MISSING_LOGO;
        }
        Integer num = getLOGO_MAP().get(make.getKey());
        return num != null ? num : MISSING_LOGO;
    }

    public List<Make> getMakes() {
        return makes;
    }

    public void setMakes(List<Make> makes) {
        this.makes = makes;
    }

    public List<Model> getModels() {
        return models;
    }

    public void setModels(List<Model> models) {
        this.models = models;
    }
}