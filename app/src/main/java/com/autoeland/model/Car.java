package com.autoeland.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.autoeland.utils.Converters;

import java.util.List;

@Entity(tableName = "car")
public class Car {
    @PrimaryKey
    private int id;
    private String title, p, vc, category, searchId;
    @TypeConverters(Converters.class)
    private List<String> details, features;
    @TypeConverters(Converters.class)
    private List<CarImage> images;
    @TypeConverters(Converters.class)
    private Contact contact;
    @TypeConverters(Converters.class)
    private List<Phone> phones;
    @TypeConverters(Converters.class)
    private List<Financing> financePlans;
    @TypeConverters(Converters.class)
    private Parking parking;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getVc() {
        return vc;
    }

    public void setVc(String vc) {
        this.vc = vc;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    public List<String> getFeatures() {
        return features;
    }

    public void setFeatures(List<String> features) {
        this.features = features;
    }

    public List<CarImage> getImages() {
        return images;
    }

    public void setImages(List<CarImage> images) {
        this.images = images;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public List<Financing> getFinancePlans() {
        return financePlans;
    }

    public void setFinancePlans(List<Financing> financePlans) {
        this.financePlans = financePlans;
    }

    public Parking getParking() {
        return parking;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }
}