package com.autoeland.model;

public class Rating {
    private String count, score, scoreLocalized, link;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getScoreLocalized() {
        return scoreLocalized;
    }

    public void setScoreLocalized(String scoreLocalized) {
        this.scoreLocalized = scoreLocalized;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}