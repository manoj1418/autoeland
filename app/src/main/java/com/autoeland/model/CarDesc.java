package com.autoeland.model;

import com.google.gson.annotations.SerializedName;

public class CarDesc {
    @SerializedName("0")
    private String location;
    @SerializedName("1")
    private String km;
    @SerializedName("2")
    private String power;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }
}