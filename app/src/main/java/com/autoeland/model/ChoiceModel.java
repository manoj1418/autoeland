package com.autoeland.model;

public class ChoiceModel {
    private String name, value, value1;
    private boolean is_checked;

    public ChoiceModel(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public ChoiceModel(String name, String value, String value1) {
        this.name = name;
        this.value = value;
        this.value1 = value1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public boolean isIs_checked() {
        return is_checked;
    }

    public void setIs_checked(boolean is_checked) {
        this.is_checked = is_checked;
    }
}