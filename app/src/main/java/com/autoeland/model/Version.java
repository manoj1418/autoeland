package com.autoeland.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.List;

@Entity(tableName = "versiones")
public class Version {
    @PrimaryKey
    @SerializedName("catalogId")
    @NonNull
    private String catalogId;
//    @SerializedName("vehicleCategory")
//    private String vehicleCategory;
//    @SerializedName("category")
//    private String category;
////    @SerializedName("features")
////    private List<String> features;
//    @SerializedName("modelDescription")
//    private String modelDescription;
//    @SerializedName("doorCount")
//    private String doorCount;
//    @SerializedName("emissionClass")
//    private String emissionClass;
//    @SerializedName("fuel")
//    private String fuel;
//    @SerializedName("power_amount")
//    private Integer power_amount;
//    @SerializedName("power_unit")
//    private String power_unit;
//    @SerializedName("transmission")
//    private String transmission;
//    @SerializedName("climatisation")
//    private String climatisation;
//    @SerializedName("numSeats")
//    private Integer numSeats;
//    @SerializedName("cubicCapacity")
//    private Integer cubicCapacity;

    @SerializedName("make_i")
    private Integer make_i;
    @SerializedName("model_i")
    private Integer model_i;

//    @SerializedName("consumptionUnit")
//    private String consumptionUnit;
//    @SerializedName("minyear")
//    private Integer minyear;
//    @SerializedName("maxyear")
//    private Integer maxyear;
//    @SerializedName("consumptionCombined")
//    private Double consumptionCombined;
//    @SerializedName("carbonDioxydEmission")
//    private Integer carbonDioxydEmission;


    @NonNull
    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(@NonNull String catalogId) {
        this.catalogId = catalogId;
    }

    public Integer getMake_i() {
        return make_i;
    }

    public void setMake_i(Integer make_i) {
        this.make_i = make_i;
    }

    public Integer getModel_i() {
        return model_i;
    }

    public void setModel_i(Integer model_i) {
        this.model_i = model_i;
    }
}