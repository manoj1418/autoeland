package com.autoeland.model;

import java.util.List;

public class CarList {
    private int numResultsTotal;
    private List<Car> items;

    public int getNumResultsTotal() {
        return numResultsTotal;
    }

    public void setNumResultsTotal(int numResultsTotal) {
        this.numResultsTotal = numResultsTotal;
    }

    public List<Car> getItems() {
        return items;
    }

    public void setItems(List<Car> items) {
        this.items = items;
    }
}