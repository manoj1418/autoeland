package com.autoeland.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.autoeland.dao.CarDao;
import com.autoeland.dao.MakeDao;
import com.autoeland.dao.ModelDao;
import com.autoeland.model.Car;
import com.autoeland.model.Make;
import com.autoeland.model.Model;

@Database(entities = {Make.class, Model.class, Car.class}, version = 1, exportSchema = false)
public abstract class AppDB extends RoomDatabase {
    private static final String DB_NAME = "Auto";
    private static AppDB appDB;

    public abstract MakeDao makeDao();

    public abstract ModelDao modelDao();

    public abstract CarDao carDao();

    public synchronized static AppDB get(final Context ctx) {
        if (appDB == null) {
            appDB = Room.databaseBuilder(ctx, AppDB.class, DB_NAME).allowMainThreadQueries().build();
        }
        return appDB;
    }
}