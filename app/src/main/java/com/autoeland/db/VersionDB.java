package com.autoeland.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.autoeland.dao.VersionDao;
import com.autoeland.model.Version;

@Database(entities = {Version.class}, version = 1, exportSchema = false)
public abstract class VersionDB extends RoomDatabase {
    private static final String DB_NAME = "Version.db";
    private static VersionDB versionDB;

    public abstract VersionDao versionDao();

    public synchronized static VersionDB get(final Context ctx) {
        if (versionDB == null) {
            versionDB =  Room.databaseBuilder(ctx, VersionDB.class, DB_NAME)
                    .createFromAsset("mobile.db")
                    .allowMainThreadQueries()
                    .build();
        }
        return versionDB;
    }
}