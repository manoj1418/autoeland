package com.autoeland.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.R;
import com.autoeland.activity.DetailActivity;
import com.autoeland.adapter.CarAdapter;
import com.autoeland.adapter.PhoneAdapter;
import com.autoeland.databinding.FragParkBinding;
import com.autoeland.db.AppDB;
import com.autoeland.model.Car;
import com.autoeland.model.Phone;
import com.autoeland.utils.CarListener;
import com.autoeland.utils.Item_Space_Decoration;
import com.autoeland.utils.PhoneListener;
import com.autoeland.utils.Retro_Fit;
import com.autoeland.utils.Utils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ParkFrag extends Fragment implements CarListener, PhoneListener {
    FragParkBinding binding;
    AppDB appDB;
    CompositeDisposable disposable;
    List<Car> carList;
    CarAdapter adapter;
    List<Phone> phones;
    PhoneAdapter phoneAdapter;

    public ParkFrag() {
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragParkBinding.inflate(inflater, container, false);

        appDB = AppDB.get(requireActivity());
        disposable = new CompositeDisposable();

        carList = new ArrayList<>();
        adapter = new CarAdapter(requireActivity(), carList, this);
        phones = new ArrayList<>();
        phoneAdapter = new PhoneAdapter(this, phones);

        binding.rvParking.setLayoutManager(new LinearLayoutManager(requireActivity()));
        binding.rvParking.addItemDecoration(new Item_Space_Decoration(16, 1, 0));
        binding.rvParking.setAdapter(adapter);
        get_data();
        return binding.getRoot();
    }

    private void get_data() {
        List<Car> temp_carList = appDB.carDao().get_all_car();
        if (temp_carList != null) {
            carList.clear();
            carList.addAll(temp_carList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_park, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (R.id.menu_sort == item.getItemId())
            Log.d("Manoj", "Hai");
        else if (R.id.menu_refresh == item.getItemId())
            Log.d("Manoj", "Hai");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (disposable != null) disposable.dispose();
        binding = null;
    }

    @Override
    public void click_car(Car car) {
        if (car != null) {
            Intent intent = new Intent(requireActivity(), DetailActivity.class);
            intent.putExtra("data", new Gson().toJson(car));
            startActivity(intent);
        }
    }

    @Override
    public void click_call(int id) {
        final AlertDialog phone_dialog = new AlertDialog.Builder(requireActivity()).create();
        final View vw_phone = View.inflate(requireActivity(), R.layout.dialog_phones, null);
        phone_dialog.setView(vw_phone);
        final RecyclerView rv_phones = vw_phone.findViewById(R.id.rv_phones);
        final SpinKitView id_progress = vw_phone.findViewById(R.id.id_progress);
        rv_phones.setLayoutManager(new LinearLayoutManager(requireActivity()));
        rv_phones.addItemDecoration(new Item_Space_Decoration(16, 1, 0));
        rv_phones.setAdapter(phoneAdapter);
        vw_phone.findViewById(R.id.btn_cancel).setOnClickListener(v -> phone_dialog.dismiss());
        phone_dialog.show();

        Utils.show_loader(id_progress);
        disposable.add(Retro_Fit.get().get_phone_numbers(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Car>() {
                    @Override
                    public void onSuccess(@NonNull Car c) {
                        Utils.hide_loader(id_progress);
                        if (c.getPhones() != null) {
                            phones.clear();
                            phones.addAll(c.getPhones());
                            phoneAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Utils.hide_loader(id_progress);
                        Toast.makeText(requireActivity(), String.valueOf(e), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    @Override
    public void click_email(Car car) {

    }

    @Override
    public void click_park(Car car, AppCompatImageView imageView, SpinKitView spinKitView) {
        if (Utils.set_parking_icon(requireActivity(), car)) {
            if (imageView != null)
                imageView.setVisibility(View.INVISIBLE);
            Utils.show_loader(spinKitView);
            appDB.carDao().delete_car(car);
            new Handler().postDelayed(() -> {
                Utils.hide_loader(spinKitView);
                if (imageView != null) {
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.ic_park_outline);
                }
                get_data();
            }, 1000);
        } else {
            if (imageView != null)
                imageView.setVisibility(View.INVISIBLE);
            Utils.show_loader(spinKitView);
            appDB.carDao().insert_car(car);
            new Handler().postDelayed(() -> {
                Utils.hide_loader(spinKitView);
                if (imageView != null) {
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.ic_park_filled);
                }
                get_data();
            }, 1000);
        }
    }

    @Override
    public void click_call(String uri) {
        if (!Utils.isEmpty(uri)) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        }
    }
}