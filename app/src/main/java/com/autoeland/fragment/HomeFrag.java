package com.autoeland.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.autoeland.R;
import com.autoeland.activity.CarListActivity;
import com.autoeland.databinding.FragHomeBinding;
import com.autoeland.model.CarList;
import com.autoeland.utils.Retro_Fit;
import com.autoeland.utils.Utils;
import com.google.gson.Gson;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class HomeFrag extends Fragment {
    FragHomeBinding binding;
    CompositeDisposable disposable;
    HashMap<String, String> params;

    public HomeFrag() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragHomeBinding.inflate(inflater, container, false);
        disposable = new CompositeDisposable();

        params = new HashMap<>();

        binding.btnSearch.setOnClickListener(v -> search_cars());
         get_search_count();
        return binding.getRoot();
    }

    private void get_search_count() {
        params = new HashMap<>();
        params.put("vc", "Car");
        params.put("ps", "0");
        params.put("psz", "0");
        params.put("dam", "0");
        params.put("sb", "rel");
        params.put("top", "");
        params.put("tic", "");

        disposable.add(Retro_Fit.get().get_search_count(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CarList>() {
                    @Override
                    public void onSuccess(@NonNull CarList cl) {
                        update_ui(String.valueOf(cl.getNumResultsTotal()));
                        Log.d("Manoj", new Gson().toJson(cl));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Manoj", String.valueOf(e));
                        Toast.makeText(requireActivity(), String.valueOf(e), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    private void update_ui(String count) {
        new Handler(Looper.getMainLooper()).post(() -> binding.btnSearch.setText(Utils.combine_str(count, "Search")));
    }

    private void search_cars() {
        Intent intent = new Intent(requireActivity(), CarListActivity.class);
        intent.putExtra("params", params);
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (disposable != null) disposable.dispose();
        binding = null;
    }
}