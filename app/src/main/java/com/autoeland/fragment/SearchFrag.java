package com.autoeland.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.autoeland.R;
import com.autoeland.adapter.TabAdapter;
import com.autoeland.databinding.FragSearchBinding;
import com.autoeland.model.Tabs;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SearchFrag extends Fragment {
    FragSearchBinding binding;
    List<Tabs> tabs;

    public SearchFrag() {
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragSearchBinding.inflate(inflater, container, false);

        tabs = new ArrayList<>();
        tabs.add(new Tabs(new CarFrag(), "Car"));
        tabs.add(new Tabs(new BikeFrag(), "Bike"));
        tabs.add(new Tabs(new MotorHomeFrag(), "Motor Home"));
        tabs.add(new Tabs(new TruckFrag(), "Truck"));

        TabAdapter adapter = new TabAdapter(requireActivity().getSupportFragmentManager(), tabs);
        binding.idViewPager.setAdapter(adapter);
        binding.idTabs.setupWithViewPager(binding.idViewPager);
        setup_icons();
        adapter.notifyDataSetChanged();
        return binding.getRoot();
    }

    private void setup_icons() {
        Objects.requireNonNull(binding.idTabs.getTabAt(0)).setIcon(R.drawable.stateful_icon_car);
        Objects.requireNonNull(binding.idTabs.getTabAt(1)).setIcon(R.drawable.stateful_icon_motorbike);
        Objects.requireNonNull(binding.idTabs.getTabAt(2)).setIcon(R.drawable.stateful_icon_motorhome);
        Objects.requireNonNull(binding.idTabs.getTabAt(3)).setIcon(R.drawable.stateful_icon_truck);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (R.id.menu_delete == item.getItemId())
            Log.d("Manoj", "Hai");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}