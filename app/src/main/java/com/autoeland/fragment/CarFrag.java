package com.autoeland.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.autoeland.activity.CarListActivity;
import com.autoeland.adapter.ConditionAdapter;
import com.autoeland.adapter.FuelAdapter;
import com.autoeland.adapter.MakeAdapter;
import com.autoeland.adapter.ModelAdapter;
import com.autoeland.adapter.SelMakeAdapter;
import com.autoeland.databinding.DialogConditionBinding;
import com.autoeland.databinding.DialogFirstRegBinding;
import com.autoeland.databinding.DialogFuelBinding;
import com.autoeland.databinding.DialogMakeModelBinding;
import com.autoeland.databinding.DialogMileageBinding;
import com.autoeland.databinding.DialogPowerBinding;
import com.autoeland.databinding.DialogPriceBinding;
import com.autoeland.databinding.FragCarBinding;
import com.autoeland.db.AppDB;
import com.autoeland.model.CarList;
import com.autoeland.model.ChoiceModel;
import com.autoeland.model.Make;
import com.autoeland.model.MakeLogos;
import com.autoeland.model.Model;
import com.autoeland.utils.ConditionListener;
import com.autoeland.utils.FuelListener;
import com.autoeland.utils.MakeListener;
import com.autoeland.utils.ModelListener;
import com.autoeland.utils.Retro_Fit;
import com.autoeland.utils.Utils;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CarFrag extends Fragment implements MakeListener, ModelListener, ConditionListener, FuelListener {
    FragCarBinding binding;
    AppDB appDB;
    CompositeDisposable disposable;
    HashMap<String, String> params;
    List<Make> makes, sel_makes;
    List<Model> models, sel_models;
    List<ChoiceModel> conditions, sel_conditions, fuels, sel_fuels, temp_choice;
    MakeAdapter makeAdapter;
    ModelAdapter modelAdapter;
    ConditionAdapter conditionAdapter;
    FuelAdapter fuelAdapter;
    SelMakeAdapter selMakeAdapter;
    AlertDialog alertDialog;
    Make make;
    boolean is_excluded = false;
    String price, year, mileage, from, to;

    public CarFrag() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragCarBinding.inflate(inflater, container, false);

        appDB = AppDB.get(requireActivity());
        disposable = new CompositeDisposable();

        params = new HashMap<>();
        makes = new ArrayList<>();
        sel_makes = new ArrayList<>();
        models = new ArrayList<>();
        sel_models = new ArrayList<>();
        conditions = new ArrayList<>();
        sel_conditions = new ArrayList<>();
        fuels = new ArrayList<>();
        sel_fuels = new ArrayList<>();
        temp_choice = new ArrayList<>();

        makeAdapter = new MakeAdapter(this, makes);
        modelAdapter = new ModelAdapter(this, models);
        conditionAdapter = new ConditionAdapter(this, conditions);
        fuelAdapter = new FuelAdapter(this, fuels);
        selMakeAdapter = new SelMakeAdapter(this, sel_makes);

        binding.rvMakeModel.setLayoutManager(new LinearLayoutManager(requireActivity()));
        binding.rvMakeModel.setAdapter(selMakeAdapter);

        binding.layMake.setOnClickListener(v -> open_dialog_make());
        binding.layCondition.setOnClickListener(v -> open_dialog_condition());
        binding.layPrice.setOnClickListener(v -> open_dialog_price());
        binding.layFirstReg.setOnClickListener(v -> open_dialog_first_reg());
        binding.layMileage.setOnClickListener(v -> open_dialog_mileage());
        binding.layPower.setOnClickListener(v -> open_dialog_power());
        binding.layFuel.setOnClickListener(v -> open_dialog_fuel());
        binding.btnSearch.setOnClickListener(v -> search_cars());

        set_data();
        return binding.getRoot();
    }


    private void set_power_data() {
//        kw
//        pw:
//        110:
//        pw:
//:150
//        pw:
//        93:150
//custom
//        pw:
//        189:289
//hp
//        pw:
//        110:
//        pw:
//:147
//        pw:
//        92:147
    }

    private void get_search_count() {
        params = new HashMap<>();
        params.put("vc", "Car");
        if (sel_makes != null && sel_makes.size() > 0) {
            for (Make m : sel_makes) {
                if (m.getModels() != null && m.getModels().size() > 0) {
                    for (Model ml : m.getModels()) {
                        params.put("ms", Utils.get_make_model(m.getKey(), ml.getKey()));
                    }
                }
            }
        }
        params.put("ps", "0");
        params.put("psz", "0");
        params.put("dam", "0");
        params.put("sb", "rel");
        params.put("top", "");
        params.put("tic", "");

//        if (dialogPriceBinding.rb500.isChecked())
//            params.put("p", "500:");
//        else if (dialogPriceBinding.rb10000.isChecked())
//            params.put("p", ":10000");
//        else if (dialogPriceBinding.rb15000.isChecked())
//            params.put("p", "15000:25000");

        disposable.add(Retro_Fit.get().get_search_count(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CarList>() {
                    @Override
                    public void onSuccess(@NonNull CarList cl) {
                        binding.btnSearch.setText(Utils.combine_str(String.valueOf(cl.getNumResultsTotal()), "Search"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Manoj", String.valueOf(e));
                        Toast.makeText(requireActivity(), String.valueOf(e), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    private void set_data() {
        List<Make> temp_make = appDB.makeDao().get_all_make();
        if (temp_make != null) {
            makes.clear();
            makes.addAll(temp_make);
            makeAdapter.notifyDataSetChanged();
        }

        conditions.clear();
        conditions.add(new ChoiceModel("New and Used", ""));
        conditions.add(new ChoiceModel("New", "NEW"));
        conditions.add(new ChoiceModel("Used", "USED"));
        conditionAdapter.notifyDataSetChanged();
        select_condition(conditions.get(0), true);

        fuels.clear();
        fuels.add(new ChoiceModel("Petrol", "PETROL"));
        fuels.add(new ChoiceModel("Diesel", "DIESEL"));
        fuels.add(new ChoiceModel("LPG", "LPG"));
        fuels.add(new ChoiceModel("Natural Gas", "CNG"));
        fuels.add(new ChoiceModel("Electric", "ELECTRICITY"));
        fuels.add(new ChoiceModel("Hybrid (petrol/electric)", "HYBRID"));
        fuels.add(new ChoiceModel("Hydrogen", "HYDROGENIUM"));
        fuels.add(new ChoiceModel("Ethanol (FFV, E85, etc.)", "ETHANOL"));
        fuels.add(new ChoiceModel("Hybrid (diesel/electric)", "HYBRID_DIESEL"));
        fuels.add(new ChoiceModel("Other", "OTHER"));
        fuelAdapter.notifyDataSetChanged();

        year = "1-" + Utils.get_year(-2) + ":12-" + Utils.get_year(0); // default year
    }


    private void open_dialog_make() {
        is_excluded = false;
        alertDialog = new AlertDialog.Builder(requireActivity()).create();
        DialogMakeModelBinding dialogMakeModelBinding = DialogMakeModelBinding.inflate(LayoutInflater.from(getContext()));
        alertDialog.setView(dialogMakeModelBinding.getRoot());
        dialogMakeModelBinding.layBottom.setVisibility(View.GONE);
        dialogMakeModelBinding.btnCancel.setVisibility(View.GONE);
        dialogMakeModelBinding.btnOk.setVisibility(View.GONE);
        dialogMakeModelBinding.txTitle.setText("Make");
        dialogMakeModelBinding.rvModel.setLayoutManager(new LinearLayoutManager(requireActivity()));
        dialogMakeModelBinding.rvModel.setAdapter(makeAdapter);
        dialogMakeModelBinding.swExclude.setOnCheckedChangeListener((buttonView, isChecked) -> is_excluded = isChecked);
        dialogMakeModelBinding.edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!Utils.isEmpty(String.valueOf(s))) {
                    List<Make> temp_makes = new ArrayList<>();
                    if (makes.size() > 0) {
                        for (Make m : makes) {
                            if (m.getValue().toLowerCase().contains(String.valueOf(s).toLowerCase()))
                                temp_makes.add(m);
                        }
                        makeAdapter.search_filter(temp_makes);
                    }
                }
            }
        });
        alertDialog.show();
    }

    private void open_dialog_model() {
        sel_models.clear();
        alertDialog = new AlertDialog.Builder(requireActivity()).create();
        DialogMakeModelBinding dialogMakeModelBinding = DialogMakeModelBinding.inflate(LayoutInflater.from(getContext()));
        alertDialog.setView(dialogMakeModelBinding.getRoot());
        dialogMakeModelBinding.txTitle.setText("Model");
        dialogMakeModelBinding.rvModel.setLayoutManager(new LinearLayoutManager(requireActivity()));
        dialogMakeModelBinding.rvModel.setAdapter(modelAdapter);
        dialogMakeModelBinding.swExclude.setChecked(is_excluded);
        dialogMakeModelBinding.edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Utils.isEmpty(String.valueOf(s))) {
                    List<Model> temp_models = new ArrayList<>();
                    if (models.size() > 0) {
                        for (Model m : models) {
                            if (m.getValue().toLowerCase().contains(String.valueOf(s).toLowerCase()))
                                temp_models.add(m);
                        }
                        modelAdapter.search_filter(temp_models);
                    }
                }
            }
        });
        dialogMakeModelBinding.btnOk.setOnClickListener(v -> {
            if (dialogMakeModelBinding.swExclude.isChecked())
                make.setValue(make.getValue() + " (excluded)");
            make.setModels(sel_models);
            sel_makes.add(make);
            selMakeAdapter.notifyDataSetChanged();
            get_search_count();
            alertDialog.dismiss();
        });
        dialogMakeModelBinding.btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        alertDialog.show();
    }

    private void open_dialog_condition() {
        if (conditions != null) {
            temp_choice.clear();
            for (ChoiceModel c : conditions) {
                c.setIs_checked(false);
                for (ChoiceModel s : sel_conditions) {
                    if (c.getName().equals(s.getName()))
                        c.setIs_checked(true);
                }
                temp_choice.add(c);
            }
            conditions.clear();
            conditions.addAll(temp_choice);
            conditionAdapter.notifyDataSetChanged();
        }
        sel_conditions.clear();
        alertDialog = new AlertDialog.Builder(requireActivity()).create();
        DialogConditionBinding dialogConditionBinding = DialogConditionBinding.inflate(LayoutInflater.from(getContext()));
        alertDialog.setView(dialogConditionBinding.getRoot());
        dialogConditionBinding.rvCondition.setLayoutManager(new LinearLayoutManager(requireActivity()));
        dialogConditionBinding.rvCondition.setAdapter(conditionAdapter);
        dialogConditionBinding.btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        alertDialog.show();
    }

    private void open_dialog_price() {
        alertDialog = new AlertDialog.Builder(requireActivity()).create();
        DialogPriceBinding dialogPriceBinding = DialogPriceBinding.inflate(LayoutInflater.from(getContext()));
        alertDialog.setView(dialogPriceBinding.getRoot());
        dialogPriceBinding.btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        dialogPriceBinding.btnOk.setOnClickListener(v -> {
            if (dialogPriceBinding.rbAny.isChecked()) {
                from = "";
                to = "";
                price = "";
            } else if (dialogPriceBinding.rb500.isChecked()) {
                from = "500";
                to = "";
                price = "500:";
            } else if (dialogPriceBinding.rb10000.isChecked()) {
                from = "";
                to = "10000";
                price = ":10000";
            } else if (dialogPriceBinding.rb15000.isChecked()) {
                from = "15000";
                to = "25000";
                price = "15000:25000";
            }
            int i_from = Utils.str_int(String.valueOf(dialogPriceBinding.edFrom.getText()));
            int i_to = Utils.str_int(String.valueOf(dialogPriceBinding.edTo.getText()));
            if (i_to < i_from) {
                dialogPriceBinding.edFrom.setText(String.valueOf(i_to));
                dialogPriceBinding.edTo.setText(String.valueOf(i_from));
            }
            if (!Utils.isEmpty(dialogPriceBinding.edFrom) && !Utils.isEmpty(dialogPriceBinding.edTo)) {
                price = dialogPriceBinding.edFrom.getText() + ":" + dialogPriceBinding.edTo.getText();
                binding.txPrice.setText(Utils.get_price_desc(String.valueOf(dialogPriceBinding.edFrom.getText()), String.valueOf(dialogPriceBinding.edTo.getText())));
            } else
                binding.txPrice.setText(Utils.get_price_desc(from, to));
            alertDialog.dismiss();
        });
        dialogPriceBinding.rgPrice.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = group.findViewById(checkedId);
            if (rb != null && rb.isChecked()) {
                dialogPriceBinding.edFrom.setText("");
                dialogPriceBinding.edTo.setText("");
            }
        });
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!Utils.isEmpty(String.valueOf(s)))
                    dialogPriceBinding.rgPrice.clearCheck();
            }
        };
        dialogPriceBinding.edFrom.addTextChangedListener(textWatcher);
        dialogPriceBinding.edTo.addTextChangedListener(textWatcher);
        alertDialog.show();
    }

    private void open_dialog_first_reg() {
//        fr:
//        1-2020:
//        fr:
//        1-2018:
//        fr:
//        1-2011:
//        fr:
//        1-2016:12-2018
        alertDialog = new AlertDialog.Builder(requireActivity()).create();
        DialogFirstRegBinding dialogFirstRegBinding = DialogFirstRegBinding.inflate(LayoutInflater.from(getContext()));
        alertDialog.setView(dialogFirstRegBinding.getRoot());
        dialogFirstRegBinding.btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        dialogFirstRegBinding.btnOk.setOnClickListener(v -> {
            if (dialogFirstRegBinding.rbAny.isChecked()) {
                from = "";
                to = "";
                year = "";
            } else if (dialogFirstRegBinding.rb1y.isChecked()) {
                from = Utils.get_year(-1);
                to = "";
                year = "1-" + Utils.get_year(-1);
            } else if (dialogFirstRegBinding.rb3y.isChecked()) {
                from = Utils.get_year(-3);
                to = "";
                year = "1-" + Utils.get_year(-3);
            } else if (dialogFirstRegBinding.rb10y.isChecked()) {
                from = Utils.get_year(-10);
                to = "";
                year = "1-" + Utils.get_year(-10);
            }
            int i_from = Utils.str_int(String.valueOf(dialogFirstRegBinding.edFrom.getText()));
            int i_to = Utils.str_int(String.valueOf(dialogFirstRegBinding.edTo.getText()));
            if (i_to < i_from) {
                dialogFirstRegBinding.edFrom.setText(String.valueOf(i_to));
                dialogFirstRegBinding.edTo.setText(String.valueOf(i_from));
            }
            if (!Utils.isEmpty(dialogFirstRegBinding.edFrom) && !Utils.isEmpty(dialogFirstRegBinding.edTo)) {
                year = "1-" + dialogFirstRegBinding.edFrom.getText() + ":12-" + dialogFirstRegBinding.edTo.getText();
                binding.txFirstReg.setText(Utils.get_first_reg_desc(String.valueOf(dialogFirstRegBinding.edFrom.getText()), String.valueOf(dialogFirstRegBinding.edTo.getText())));
            } else
                binding.txFirstReg.setText(Utils.get_first_reg_desc(from, to));
            alertDialog.dismiss();
        });
        dialogFirstRegBinding.rgReg.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = group.findViewById(checkedId);
            if (rb != null && rb.isChecked()) {
                dialogFirstRegBinding.edFrom.setText("");
                dialogFirstRegBinding.edTo.setText("");
            }
        });
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!Utils.isEmpty(String.valueOf(s)))
                    dialogFirstRegBinding.rgReg.clearCheck();
            }
        };
        dialogFirstRegBinding.edFrom.addTextChangedListener(textWatcher);
        dialogFirstRegBinding.edTo.addTextChangedListener(textWatcher);
        alertDialog.show();
    }

    private void open_dialog_mileage() {
//        ml:
//        5000:
//        ml:
//        5000:10000
//        ml:
//        :150000
//        ml:
//        100:150
        alertDialog = new AlertDialog.Builder(requireActivity()).create();
        DialogMileageBinding dialogMileageBinding = DialogMileageBinding.inflate(LayoutInflater.from(getContext()));
        alertDialog.setView(dialogMileageBinding.getRoot());
        dialogMileageBinding.btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        dialogMileageBinding.btnOk.setOnClickListener(v -> {
            if (dialogMileageBinding.rbAny.isChecked()) {
                from = "";
                to = "";
                mileage = "";
            } else if (dialogMileageBinding.rb5000.isChecked()) {
                from = "5000";
                to = "";
                mileage = "5000:";
            } else if (dialogMileageBinding.rb10000.isChecked()) {
                from = "5000";
                to = "10000";
                mileage = "5000:10000";
            } else if (dialogMileageBinding.rb150000.isChecked()) {
                from = "";
                to = "150000";
                mileage = ":150000";
            }
            int i_from = Utils.str_int(String.valueOf(dialogMileageBinding.edFrom.getText()));
            int i_to = Utils.str_int(String.valueOf(dialogMileageBinding.edTo.getText()));
            if (i_to < i_from) {
                dialogMileageBinding.edFrom.setText(String.valueOf(i_to));
                dialogMileageBinding.edTo.setText(String.valueOf(i_from));
            }
            if (!Utils.isEmpty(dialogMileageBinding.edFrom) && !Utils.isEmpty(dialogMileageBinding.edTo)) {
                mileage = dialogMileageBinding.edFrom.getText() + ":" + dialogMileageBinding.edTo.getText();
                binding.txMileage.setText(Utils.get_mileage_desc(String.valueOf(dialogMileageBinding.edFrom.getText()), String.valueOf(dialogMileageBinding.edTo.getText())));
            } else
                binding.txMileage.setText(Utils.get_mileage_desc(from, to));
            alertDialog.dismiss();
        });
        dialogMileageBinding.rgMileage.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = group.findViewById(checkedId);
            if (rb != null && rb.isChecked()) {
                dialogMileageBinding.edFrom.setText("");
                dialogMileageBinding.edTo.setText("");
            }
        });
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!Utils.isEmpty(String.valueOf(s)))
                    dialogMileageBinding.rgMileage.clearCheck();
            }
        };
        dialogMileageBinding.edFrom.addTextChangedListener(textWatcher);
        dialogMileageBinding.edTo.addTextChangedListener(textWatcher);
        alertDialog.show();
    }

    private void open_dialog_power() {
        alertDialog = new AlertDialog.Builder(requireActivity()).create();
        DialogPowerBinding dialogPowerBinding = DialogPowerBinding.inflate(LayoutInflater.from(getContext()));
        alertDialog.setView(dialogPowerBinding.getRoot());
        dialogPowerBinding.btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        dialogPowerBinding.btnOk.setOnClickListener(v -> alertDialog.dismiss());
        alertDialog.show();
    }

    private void open_dialog_fuel() {
        if (fuels != null) {
            temp_choice.clear();
            for (ChoiceModel f : fuels) {
                f.setIs_checked(false);
                for (ChoiceModel s : sel_fuels) {
                    if (f.getName().equals(s.getName()))
                        f.setIs_checked(true);
                }
                temp_choice.add(f);
            }
            fuels.clear();
            fuels.addAll(temp_choice);
            fuelAdapter.notifyDataSetChanged();
        }
        alertDialog = new AlertDialog.Builder(requireActivity()).create();
        DialogFuelBinding dialogFuelBinding = DialogFuelBinding.inflate(LayoutInflater.from(getContext()));
        alertDialog.setView(dialogFuelBinding.getRoot());
        dialogFuelBinding.rvFuel.setLayoutManager(new LinearLayoutManager(requireActivity()));
        dialogFuelBinding.rvFuel.setAdapter(fuelAdapter);
        dialogFuelBinding.btnOk.setOnClickListener(v -> {
            binding.txFuel.setText(Utils.get_choice(sel_fuels));
            alertDialog.dismiss();
        });
        dialogFuelBinding.btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        alertDialog.show();
    }

    private void search_cars() {
        Intent intent = new Intent(requireActivity(), CarListActivity.class);
        intent.putExtra("params", params);
        startActivity(intent);
    }

    @Override
    public void click_make(Make mk) {
        make = new Make(mk.getKey(), mk.getValue(), mk.getRes_id());
        List<Model> ml = new Gson().fromJson(Utils.check_str(load_local("models_for_make_" + mk.getKey())), MakeLogos.class).getModels();
        if (ml != null) {
            models.clear();
            models.addAll(ml);
            modelAdapter.notifyDataSetChanged();
        }
        if (alertDialog != null) {
            alertDialog.dismiss();
            open_dialog_model();
        }
    }

    @Override
    public void delete_make(Make mk) {
        if (mk != null) {
            sel_makes.remove(mk);
            selMakeAdapter.notifyDataSetChanged();
            get_search_count();
        }
    }

    @Override
    public void insert_model(Model m) {
        if (m != null) sel_models.add(m);
    }

    @Override
    public void delete_model(Model m) {
        if (m != null) sel_models.remove(m);
    }

    @Override
    public void select_condition(ChoiceModel cm, boolean is_checked) {
        if (cm != null) {
            if (is_checked) sel_conditions.add(cm);
            else sel_conditions.remove(cm);
        }
        binding.txConditions.setText(Utils.get_choice(sel_conditions));
        if (alertDialog != null) alertDialog.dismiss();
    }

    @Override
    public void select_fuel(ChoiceModel cm, boolean is_checked) {
        if (cm != null) {
            if (is_checked) sel_fuels.add(cm);
            else sel_fuels.remove(cm);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (disposable != null) disposable.dispose();
        binding = null;
    }

    private String load_local(String fname) {
        String json = null;
        try {
            InputStream is = requireActivity().getAssets().open(fname);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}