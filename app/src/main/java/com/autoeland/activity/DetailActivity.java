package com.autoeland.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.R;
import com.autoeland.adapter.FeaturesAdapter;
import com.autoeland.adapter.PhoneAdapter;
import com.autoeland.databinding.ActivityDetailBinding;
import com.autoeland.model.AdsDetails;
import com.autoeland.model.Car;
import com.autoeland.model.CarImage;
import com.autoeland.model.Phone;
import com.autoeland.utils.Item_Space_Decoration;
import com.autoeland.utils.PhoneListener;
import com.autoeland.utils.Retro_Fit;
import com.autoeland.utils.Utils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class DetailActivity extends AppCompatActivity implements PhoneListener {
    ActivityDetailBinding binding;
    CompositeDisposable disposable;
    Car car;
    List<CarImage> carImages;
    List<String> features;
    List<Phone> phones;
    PhoneAdapter phoneAdapter;
    FeaturesAdapter featuresAdapter;
    String financing_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        disposable = new CompositeDisposable();

        carImages = new ArrayList<>();
        features = new ArrayList<>();
        featuresAdapter = new FeaturesAdapter(features);
        phones = new ArrayList<>();
        phoneAdapter = new PhoneAdapter(this, phones);

        binding.idToolbar.setTitle(getResources().getString(R.string.tx_search));
        setSupportActionBar(binding.idToolbar);
        binding.idToolbar.setNavigationIcon(R.drawable.ic_back);
        binding.idToolbar.setNavigationOnClickListener(v -> onBackPressed());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String content = bundle.getString("data");
            if (content != null) {
                car = new Gson().fromJson(content, Car.class);
                if (car != null) {
                    set_data();
                    get_data();
                }
            }
        }

        binding.rvFeatures.setLayoutManager(new GridLayoutManager(this, 2));
        binding.rvFeatures.addItemDecoration(new Item_Space_Decoration(16, 2, 0));
        binding.rvFeatures.setAdapter(featuresAdapter);
        binding.idBanner.setImageListener((position, imageView) -> Utils.set_image(Utils.set_image_url(carImages.get(position).getUri()), imageView));
        binding.btnCall.setOnClickListener(v -> click_call());
        binding.txFinancing.setOnClickListener(v -> open_compare_quotes());
    }

    private void open_compare_quotes() {
        if (!Utils.isEmpty(financing_url))
            startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(financing_url)));
    }

    private void get_data() {
        if (car.getId() != 0 && !Utils.isEmpty(car.getSearchId())) {
            Utils.show_loader(binding.idProgress);
            disposable.add(Retro_Fit.get().get_ads_details(car.getId(), true, car.getSearchId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<AdsDetails>() {
                        @Override
                        public void onSuccess(@NonNull AdsDetails adsDetails) {
                            Utils.hide_loader(binding.idProgress);
                            if (adsDetails.getFinancePlans() != null && adsDetails.getFinancePlans().size() > 0)
                                financing_url = Utils.check_str(adsDetails.getFinancePlans().get(0).getUrl());
                            if (!Utils.isEmpty(adsDetails.getHtmlDescription())) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                                    binding.txDesc.setText(Html.fromHtml(adsDetails.getHtmlDescription(), Html.FROM_HTML_MODE_COMPACT));
                                else
                                    binding.txDesc.setText(Html.fromHtml(adsDetails.getHtmlDescription()));
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Utils.hide_loader(binding.idProgress);
                            Toast.makeText(DetailActivity.this, String.valueOf(e), Toast.LENGTH_SHORT).show();
                        }
                    }));
        }
    }

    private void set_data() {
        if (car.getFinancePlans() != null && car.getFinancePlans().size() > 0)
            financing_url = Utils.check_str(car.getFinancePlans().get(0).getUrl());
        binding.txTitle.setText(Utils.check_str(car.getTitle()));
        binding.txPrice.setText(Utils.check_str(car.getP()));
        binding.txDesc.setText(Utils.check_str(Utils.get_desc(car.getDetails())));
        binding.txLocation.setText(Utils.check_str(car.getDetails() != null && car.getDetails().size() > 0 ? car.getDetails().get(0) : ""));
        if (car.getImages() != null && car.getImages().size() > 0) {
            carImages.clear();
            carImages.addAll(car.getImages());
            binding.idBanner.setPageCount(carImages.size());
        }
        if (car.getFeatures() != null && car.getFeatures().size() > 0) {
            features.clear();
            features.addAll(car.getFeatures());
            featuresAdapter.notifyDataSetChanged();
        }
    }

    public void click_call() {
        if (car != null && car.getId() != 0) {
            final AlertDialog phone_dialog = new AlertDialog.Builder(DetailActivity.this).create();
            final View vw_phone = View.inflate(DetailActivity.this, R.layout.dialog_phones, null);
            phone_dialog.setView(vw_phone);
            final RecyclerView rv_phones = vw_phone.findViewById(R.id.rv_phones);
            final SpinKitView id_progress = vw_phone.findViewById(R.id.id_progress);
            rv_phones.setLayoutManager(new LinearLayoutManager(DetailActivity.this));
            rv_phones.addItemDecoration(new Item_Space_Decoration(16, 1, 0));
            rv_phones.setAdapter(phoneAdapter);
            vw_phone.findViewById(R.id.btn_cancel).setOnClickListener(v -> phone_dialog.dismiss());
            phone_dialog.show();

            Utils.show_loader(id_progress);
            disposable.add(Retro_Fit.get().get_phone_numbers(car.getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<Car>() {
                        @Override
                        public void onSuccess(@NonNull Car c) {
                            Utils.hide_loader(id_progress);
                            if (c.getPhones() != null && c.getPhones().size() > 0) {
                                phones.clear();
                                phones.addAll(c.getPhones());
                                phoneAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Utils.hide_loader(id_progress);
                            Toast.makeText(DetailActivity.this, String.valueOf(e), Toast.LENGTH_SHORT).show();
                        }
                    }));
        }
    }

    @Override
    public void click_call(String uri) {
        if (!Utils.isEmpty(uri)) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        }

//        {
//            "324315237": "DELETED"
//        }
    }

    private void share_car_data(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        if (car != null) {
            intent.putExtra(Intent.EXTRA_TEXT, car.getTitle());
            intent.putExtra(Intent.EXTRA_SUBJECT, car.getP());
        }
        startActivity(Intent.createChooser(intent, getString(R.string.tx_share)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (R.id.menu_share == item.getItemId())
            share_car_data();
//        else if (R.id.menu_sort == item.getItemId())
//            open_dialog_sorting();
//        else if (R.id.menu_save_search == item.getItemId())
//            open_dialog_save_search();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}