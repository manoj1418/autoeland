package com.autoeland.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.autoeland.R;
import com.autoeland.databinding.ActivityMainBinding;
import com.autoeland.databinding.NavigationFooterBinding;
import com.autoeland.databinding.NavigationLayoutBinding;
import com.autoeland.db.AppDB;
import com.autoeland.fragment.HomeFrag;
import com.autoeland.fragment.ParkFrag;
import com.autoeland.fragment.SearchFrag;
import com.autoeland.model.CarList;
import com.autoeland.model.Make;
import com.autoeland.model.MakeLogos;
import com.autoeland.model.ResponseModel;
import com.autoeland.utils.Retro_Fit;
import com.autoeland.utils.Utils;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    ActivityMainBinding binding;
    NavigationLayoutBinding nav_binding;
    NavigationFooterBinding footer_binding;
    AppDB appDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        nav_binding = NavigationLayoutBinding.bind(binding.navView.getRoot());
        footer_binding = NavigationFooterBinding.bind(nav_binding.footerView.getRoot());
        setContentView(binding.getRoot());

        appDB = AppDB.get(this);

        binding.idToolbar.setTitle(getResources().getString(R.string.app_name));
        setSupportActionBar(binding.idToolbar);
        binding.idToolbar.setNavigationIcon(R.drawable.ic_menu);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, binding.idDrawer, binding.idToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.idDrawer.addDrawerListener(toggle);
        toggle.syncState();

        nav_binding.idNv.setNavigationItemSelectedListener(this);
        cur_frag(R.id.nav_home);
        get_data();

        footer_binding.txLogin.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, LoginActivity.class)));
//        Type mapType = new TypeToken<Map<Integer, Result> >() {}.getType(); // define generic type
//        Map<Integer, Result> result= gson.fromJson(new InputStreamReader(source), mapType);

//        Parking p = new Parking();
//        try {
//            Log.d("Manoj", new Gson().toJson(p.getClass().getTypeName()));
//            Log.d("Manoj", new Gson().toJson(p.getClass().getDeclaredFields()));
//            Field f = p.getClass().getFields("result");
//            f.setAccessible(true);
//            f.set(p, "hai");
//        } catch (NoSuchFieldException | IllegalAccessException e) {
//            e.printStackTrace();
//        }
//        Log.d("Manoj", new Gson().toJson(p));

//        Log.d("Manoj", new GsonBuilder().setFieldNamingStrategy(f -> f.getName().equals("result") ? "hai" : f.getName()).create().toJson(new Parking("250")));
    }


    private void get_data() {
        set_data(new Gson().fromJson(Utils.check_str(load_local("makes_for_car")), MakeLogos.class).getMakes());
    }

    private void set_data(List<Make> mk) {
        if (mk != null && mk.size() > 0) {
            List<Make> makes = new ArrayList<>();
            for (Make m : mk) {
                m.setRes_id(MakeLogos.getLogo(m));
                makes.add(m);
            }
            appDB.makeDao().insert_make(makes);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        cur_frag(item.getItemId());
        return false;
    }

    public void cur_frag(int id) {
        if (id == R.id.nav_home)
            set_fragment(new HomeFrag());
        else if (id == R.id.nav_search)
            set_fragment(new SearchFrag());
        else if (id == R.id.nav_search_accessories)
            startActivity(new Intent(MainActivity.this, SearchAccessoriesActivity.class));
        else if (id == R.id.nav_park)
            set_fragment(new ParkFrag());
        else if (id == R.id.nav_my_search)
            set_fragment(new HomeFrag());
        else if (id == R.id.nav_message)
            startActivity(new Intent(MainActivity.this, ChatActivity.class));
        else if (id == R.id.nav_notification)
            set_fragment(new HomeFrag());
        else if (id == R.id.nav_sell)
//            set_fragment(new HomeFrag());
            startActivity(new Intent(MainActivity.this, SellActivity.class));
        else if (id == R.id.nav_car_valuation)
            set_fragment(new HomeFrag());
        else if (id == R.id.nav_financing)
            set_fragment(new HomeFrag());
        else if (id == R.id.nav__help)
            set_fragment(new HomeFrag());
        binding.idDrawer.closeDrawer(GravityCompat.START);
    }

    private void set_fragment(Fragment frag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.id_frame, frag);
        ft.commit();
    }

    private String load_local(String fname) {
        String json = null;
        try {
            InputStream is = getAssets().open(fname);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new CompositeDisposable().add(Retro_Fit.get().login()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ResponseModel>() {
                    @Override
                    public void onSuccess(@NonNull ResponseModel rm) {
                        Log.d("Manoj", new Gson().toJson(rm));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.d("Manoj", String.valueOf(e));
                        Toast.makeText(MainActivity.this, String.valueOf(e), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    @Override
    public void onBackPressed() {
        if (binding.idDrawer.isDrawerOpen(GravityCompat.START)) {
            binding.idDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}