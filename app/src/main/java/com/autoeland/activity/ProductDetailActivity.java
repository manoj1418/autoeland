package com.autoeland.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import androidx.appcompat.app.AppCompatActivity;

import com.autoeland.R;
import com.autoeland.databinding.ActivityProductDetailBinding;
import com.autoeland.model.Image;
import com.autoeland.model.Product;
import com.autoeland.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class ProductDetailActivity extends AppCompatActivity {
    ActivityProductDetailBinding binding;
    CompositeDisposable disposable;
    Product product;
    List<Image> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProductDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        disposable = new CompositeDisposable();

        images = new ArrayList<>();

        binding.idToolbar.setTitle(getResources().getString(R.string.tx_pro_access));
        setSupportActionBar(binding.idToolbar);
        binding.idToolbar.setNavigationIcon(R.drawable.ic_back);
        binding.idToolbar.setNavigationOnClickListener(v -> onBackPressed());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String content = bundle.getString("data");
            if (content != null) {
                product = new Gson().fromJson(content, Product.class);
                if (product != null) {
                    set_data();
                    get_data();
                }
            }
        }

        binding.idBanner.setImageListener((position, imageView) -> {
            Log.d("Manoj", images.get(position).getMedium());
            Utils.set_image(images.get(position).getMedium(), imageView);
        });
    }

    private void get_data() {

    }

    private void set_data() {
        if (product.getImages() != null && product.getImages().size() > 0) {
            images.clear();
            images.addAll(product.getImages());
            binding.idBanner.setPageCount(images.size());
        }
        binding.txPrice.setText(Utils.get_currency_symbol(product.getCurrency(), Utils.get_currency_format(product.getPrice())));
        binding.txTitle.setText(product.getTitle());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}