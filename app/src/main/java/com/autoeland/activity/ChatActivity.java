package com.autoeland.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.autoeland.R;
import com.autoeland.adapter.ChatAdapter;
import com.autoeland.databinding.ActivityChatBinding;
import com.autoeland.model.ChatModel;
import com.autoeland.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class ChatActivity extends AppCompatActivity {
    ActivityChatBinding binding;
    CompositeDisposable disposable;
    List<ChatModel> chatModels;
    ChatAdapter chatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChatBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        disposable = new CompositeDisposable();

        chatModels = new ArrayList<>();
        chatModels.add(new ChatModel("0", "Hello, how can i help you ?", new Date()));
        chatAdapter = new ChatAdapter(ChatActivity.this, chatModels);

        binding.rvChat.setHasFixedSize(true);
        LinearLayoutManager lm = new LinearLayoutManager(ChatActivity.this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        lm.setStackFromEnd(true);
        lm.setSmoothScrollbarEnabled(true);
        lm.setReverseLayout(false);
        binding.rvChat.setLayoutManager(lm);
        binding.rvChat.setAdapter(chatAdapter);

        binding.edMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() == 0) {
                    binding.btnSend.setEnabled(false);
                    binding.btnSend.setTextColor(getResources().getColor(R.color.shadow));
                } else {
                    binding.btnSend.setEnabled(true);
                    binding.btnSend.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        binding.btnSend.setOnClickListener(v -> {
            chatModels.add(new ChatModel(Utils.get_user(ChatActivity.this), String.valueOf(binding.edMsg.getText()), new Date()));
            chatAdapter.notifyDataSetChanged();
            binding.edMsg.setText("");
            binding.edMsg.requestFocus();
            binding.rvChat.smoothScrollToPosition(chatAdapter.getItemCount());
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposable != null) disposable.dispose();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}