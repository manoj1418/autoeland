package com.autoeland.activity;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import com.autoeland.R;
import com.autoeland.adapter.SelPicAdapter;
import com.autoeland.databinding.ActivityImageSelectionBinding;
import com.autoeland.model.ResponseModel;
import com.autoeland.utils.Item_Space_Decoration;
import com.autoeland.utils.Retro_Fit_2;
import com.autoeland.utils.Utils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.adapter.image.impl.PicassoAdapter;
import com.sangcomz.fishbun.define.Define;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ImageSelectionActivity extends AppCompatActivity {
    ActivityImageSelectionBinding binding;
    CompositeDisposable disposable;
    List<Uri> sel_uri;
    SelPicAdapter selPicAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityImageSelectionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        disposable = new CompositeDisposable();
        sel_uri = new ArrayList<>();
        selPicAdapter = new SelPicAdapter(sel_uri);

        binding.idToolbar.setTitle(getResources().getString(R.string.tx_search));
        setSupportActionBar(binding.idToolbar);
        binding.idToolbar.setNavigationIcon(R.drawable.ic_back);
        binding.idToolbar.setNavigationOnClickListener(v -> onBackPressed());

        binding.rvPic.setLayoutManager(new GridLayoutManager(ImageSelectionActivity.this, 2));
        binding.rvPic.addItemDecoration(new Item_Space_Decoration(16, 2, 0));
        binding.rvPic.setAdapter(selPicAdapter);

        binding.btnSelPic.setOnClickListener(v -> check_permission());
        binding.btnUpload.setOnClickListener(v -> upload_images());
    }

    private void check_permission() {
        Dexter.withContext(ImageSelectionActivity.this)
                .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (sel_uri.size() > 10) sel_uri.clear();
                            open_gallery();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void open_gallery() {
        FishBun.with(ImageSelectionActivity.this)
                .setImageAdapter(new PicassoAdapter())
                .setIsUseDetailView(false)
                .setMinCount(1)
                .setMaxCount(10)
                .setPickerSpanCount(3)
                .setAlbumSpanCount(1, 2)
                .setCamera(true)
                .exceptGif(true)
                .setButtonInAlbumActivity(false)
                .setReachLimitAutomaticClose(false)
                .setAllViewTitle("All")
                .textOnImagesSelectionLimitReached("Select only one picture")
                .textOnNothingSelected("Please select at least one picture")
                .setActionBarTitle(getResources().getString(R.string.app_name))
                .setActionBarTitleColor(ContextCompat.getColor(ImageSelectionActivity.this, R.color.white))
                .setSelectCircleStrokeColor(ContextCompat.getColor(ImageSelectionActivity.this, R.color.white))
                .setActionBarColor(ContextCompat.getColor(ImageSelectionActivity.this, R.color.colorPrimary), ContextCompat.getColor(ImageSelectionActivity.this, R.color.colorPrimaryDark), false)
                .setOkButtonDrawable(ContextCompat.getDrawable(ImageSelectionActivity.this, R.drawable.ic_done))
                .setHomeAsUpIndicatorDrawable(ContextCompat.getDrawable(ImageSelectionActivity.this, R.drawable.ic_back))
                .startAlbum();
    }

    private void crop_image(ArrayList<Uri> uris) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(90);
        options.setHideBottomControls(true);
        options.setDimmedLayerColor(Color.BLACK);
        options.setToolbarCropDrawable(R.drawable.ic_done);
        options.setToolbarCancelDrawable(R.drawable.ic_close);
        options.setCropFrameColor(ContextCompat.getColor(ImageSelectionActivity.this, R.color.colorPrimary));
        options.setCropGridColor(ContextCompat.getColor(ImageSelectionActivity.this, R.color.colorPrimary));
        options.setToolbarColor(ContextCompat.getColor(ImageSelectionActivity.this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(ImageSelectionActivity.this, R.color.colorPrimaryDark));
        options.setActiveWidgetColor(ContextCompat.getColor(ImageSelectionActivity.this, R.color.white));
        options.setToolbarWidgetColor(ContextCompat.getColor(ImageSelectionActivity.this, R.color.white));
        for (Uri uri : uris) {
            UCrop.of(uri, Uri.fromFile(new File(getExternalCacheDir(), queryName(uri))))
                    .withAspectRatio(4, 3)
                    .withMaxResultSize(500, 500)
                    .withOptions(options)
                    .start(ImageSelectionActivity.this);
        }
    }

    private String queryName(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        assert cursor != null;
        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        cursor.moveToFirst();
        String name = cursor.getString(nameIndex);
        cursor.close();
        return name;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Define.ALBUM_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    crop_image(Objects.requireNonNull(data.getParcelableArrayListExtra(Define.INTENT_PATH)));
                }
                break;
            case UCrop.REQUEST_CROP:
                if (resultCode == RESULT_OK) {
                    if (data != null) sel_uri.add(UCrop.getOutput(data));
                    selPicAdapter.notifyDataSetChanged();
                }
                break;
            case UCrop.RESULT_ERROR:
                if (data != null) {
                    Log.d("Manoj", String.valueOf(UCrop.getError(data)));
                }
                break;
        }
    }

    private void upload_images() {
        for (Uri u : sel_uri) {
            Log.d("Manoj", u.getPath());
            File file = new File(u.getPath());
            Log.d("Manoj", file.getPath());
            RequestBody requestBody = RequestBody.create(file, MediaType.parse("image/jpeg"));
            MultipartBody.Part body = MultipartBody.Part.create(requestBody);
            disposable.add(Retro_Fit_2.get().upload_images(body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<ResponseModel>() {
                        @Override
                        public void onSuccess(@NonNull ResponseModel rm) {
                            Log.d("Manoj", Utils.set_image_url(rm.getUri()));
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Log.d("Manoj", String.valueOf(e));
                            Toast.makeText(ImageSelectionActivity.this, String.valueOf(e), Toast.LENGTH_SHORT).show();
                        }
                    }));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}