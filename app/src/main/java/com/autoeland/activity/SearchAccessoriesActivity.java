package com.autoeland.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import com.autoeland.R;
import com.autoeland.adapter.ProductAdapter;
import com.autoeland.databinding.ActivitySearchAccessoriesBinding;
import com.autoeland.databinding.LaySortByBinding;
import com.autoeland.model.Product;
import com.autoeland.model.ProductList;
import com.autoeland.utils.Grid_Space_Decoration;
import com.autoeland.utils.ProductListener;
import com.autoeland.utils.Retro_Fit_3;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.chip.Chip;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SearchAccessoriesActivity extends AppCompatActivity implements ProductListener {
    ActivitySearchAccessoriesBinding binding;
    LaySortByBinding laySortByBinding;
    CompositeDisposable disposable;
    List<Product> products;
    ProductAdapter productAdapter;
    List<String> filters;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySearchAccessoriesBinding.inflate(getLayoutInflater());
        laySortByBinding = binding.laySortBy;
        setContentView(binding.getRoot());

        disposable = new CompositeDisposable();

        filters = new ArrayList<>();
        filters.add("Price");
        filters.add("Condition");
        filters.add("Brand");

        products = new ArrayList<>();
        productAdapter = new ProductAdapter(this, products);

        binding.idToolbar.setTitle(getResources().getString(R.string.tx_pro_access));
        setSupportActionBar(binding.idToolbar);
        binding.idToolbar.setNavigationIcon(R.drawable.ic_back);
        binding.idToolbar.setNavigationOnClickListener(v -> onBackPressed());

        binding.rvProduct.setLayoutManager(new GridLayoutManager(this, 2));
        binding.rvProduct.addItemDecoration(new Grid_Space_Decoration(2, 24, true));
        binding.rvProduct.setAdapter(productAdapter);

        binding.txSorting.setOnClickListener(v -> {
            if (type != null && type.equals("sort")) {
                type = null;
                hide_bottom_sheet();
            } else {
                type = "sort";
                set_bottom_sheet(laySortByBinding.laySortBy, 50);
            }
        });
        hide_bottom_sheet();
        add_qty_chip();
        get_data();
    }

    private void get_data() {
        disposable.add(Retro_Fit_3.get().get_product_list(12465, 0, 12.8996, 80.2209, 0, 4, 56, "60b8b003-62b9-4f69-8a00-6baadc245d2c", "category_slider")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ProductList>() {
                    @Override
                    public void onSuccess(@NonNull ProductList pl) {
                        if (pl.getSearch_objects() != null) {
                            products.clear();
                            products.addAll(pl.getSearch_objects());
                            productAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(SearchAccessoriesActivity.this, String.valueOf(e), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    private void hide_bottom_sheet() {
        set_bottom_sheet(laySortByBinding.laySortBy, 0);
    }

    private void set_bottom_sheet(ConstraintLayout cons_lay, int percentage) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = (displayMetrics.heightPixels / 100) * percentage;
        cons_lay.setMaxHeight(height);
        BottomSheetBehavior btm_sheet = BottomSheetBehavior.from(cons_lay);
        btm_sheet.setHideable(false);
        btm_sheet.setPeekHeight(0);
        btm_sheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        btm_sheet.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                if (newState == BottomSheetBehavior.STATE_DRAGGING)
//                    btm_sheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void add_qty_chip() {
        int count = 0;
        binding.chipFilter.removeAllViews();
        for (String s : filters) {
            Chip chip = new Chip(SearchAccessoriesActivity.this);
            chip.setChipBackgroundColor(get_color_bg());
            chip.setText(s);
            chip.setTextColor(get_color_text());
            chip.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            chip.setCheckable(true);
            chip.setChipIcon(ContextCompat.getDrawable(SearchAccessoriesActivity.this, R.drawable.ic_down));
            chip.setChipIconTint(get_color_stroke());
            chip.setCheckedIcon(ContextCompat.getDrawable(SearchAccessoriesActivity.this, R.drawable.ic_down));
            chip.setChipStrokeWidth(2.5f);
            chip.setChipStrokeColor(get_color_stroke());
            chip.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                }
            });
            binding.chipFilter.addView(chip);
            if (count == 0) chip.setChecked(true);
            count++;
        }
    }

    private ColorStateList get_color_bg() {
        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_enabled, android.R.attr.state_selected},
                        new int[]{android.R.attr.state_enabled},
                        new int[]{}
                },
                new int[]{
                        ContextCompat.getColor(SearchAccessoriesActivity.this, R.color.colorPrimary),
                        ContextCompat.getColor(SearchAccessoriesActivity.this, R.color.white),
                        ContextCompat.getColor(SearchAccessoriesActivity.this, R.color.white)
                }
        );
    }

    private ColorStateList get_color_text() {
        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_enabled, android.R.attr.state_selected},
                        new int[]{android.R.attr.state_enabled},
                        new int[]{}
                },
                new int[]{
                        ContextCompat.getColor(SearchAccessoriesActivity.this, R.color.white),
                        ContextCompat.getColor(SearchAccessoriesActivity.this, R.color.colorPrimary),
                        ContextCompat.getColor(SearchAccessoriesActivity.this, R.color.colorPrimary)
                }
        );
    }

    private ColorStateList get_color_stroke() {
        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_enabled, android.R.attr.state_selected},
                        new int[]{android.R.attr.state_enabled},
                        new int[]{}
                },
                new int[]{
                        ContextCompat.getColor(SearchAccessoriesActivity.this, R.color.white),
                        ContextCompat.getColor(SearchAccessoriesActivity.this, R.color.colorPrimary),
                        ContextCompat.getColor(SearchAccessoriesActivity.this, R.color.colorPrimary)
                }
        );
    }

    @Override
    public void open_product(Product product) {
        if (product != null) {
            Intent intent = new Intent(SearchAccessoriesActivity.this, ProductDetailActivity.class);
            intent.putExtra("data", new Gson().toJson(product));
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}