package com.autoeland.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.autoeland.R;
import com.autoeland.adapter.CarAdapter;
import com.autoeland.adapter.PhoneAdapter;
import com.autoeland.databinding.ActivityCarListBinding;
import com.autoeland.db.AppDB;
import com.autoeland.model.Car;
import com.autoeland.model.CarList;
import com.autoeland.model.ChoiceModel;
import com.autoeland.model.Phone;
import com.autoeland.utils.CarListener;
import com.autoeland.utils.Item_Space_Decoration;
import com.autoeland.utils.PhoneListener;
import com.autoeland.utils.Retro_Fit;
import com.autoeland.utils.Utils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CarListActivity extends AppCompatActivity implements CarListener, PhoneListener {
    ActivityCarListBinding binding;
    AppDB appDB;
    CompositeDisposable disposable;
    List<Car> carList;
    CarAdapter adapter;
    List<Phone> phones;
    PhoneAdapter phoneAdapter;
    List<ChoiceModel> choiceModels;
    HashMap<String, String> params;
    AlertDialog dialog_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCarListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        appDB = AppDB.get(CarListActivity.this);
        disposable = new CompositeDisposable();

        carList = new ArrayList<>();
        choiceModels = new ArrayList<>();
        adapter = new CarAdapter(CarListActivity.this, carList, this);
        phones = new ArrayList<>();
        phoneAdapter = new PhoneAdapter(this, phones);
        params = new HashMap<>();

        binding.idToolbar.setTitle(getResources().getString(R.string.tx_search));
        setSupportActionBar(binding.idToolbar);
        binding.idToolbar.setNavigationIcon(R.drawable.ic_back);
        binding.idToolbar.setNavigationOnClickListener(v -> onBackPressed());

        if (getIntent().getSerializableExtra("params") != null)
            params = (HashMap<String, String>) getIntent().getSerializableExtra("params");

        binding.rvCar.setLayoutManager(new LinearLayoutManager(this));
        binding.rvCar.addItemDecoration(new Item_Space_Decoration(16, 1, 0));
        binding.rvCar.setAdapter(adapter);
        set_sorting_data();
        get_data();
    }

    private void get_data() {
        params.put("psz", "20");
        disposable.add(Retro_Fit.get().get_data(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CarList>() {
                    @Override
                    public void onSuccess(@NonNull CarList cl) {
                        if (cl.getItems() != null) {
                            carList.clear();
                            carList.addAll(cl.getItems());
                            adapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(CarListActivity.this, String.valueOf(e), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    @Override
    public void click_car(Car car) {
        if (car != null) {
            Intent intent = new Intent(CarListActivity.this, DetailActivity.class);
            intent.putExtra("data", new Gson().toJson(car));
            startActivity(intent);
        }
    }

    @Override
    public void click_call(int id) {
        final AlertDialog phone_dialog = new AlertDialog.Builder(CarListActivity.this).create();
        final View vw_phone = View.inflate(CarListActivity.this, R.layout.dialog_phones, null);
        phone_dialog.setView(vw_phone);
        final RecyclerView rv_phones = vw_phone.findViewById(R.id.rv_phones);
        final SpinKitView id_progress = vw_phone.findViewById(R.id.id_progress);
        rv_phones.setLayoutManager(new LinearLayoutManager(CarListActivity.this));
        rv_phones.addItemDecoration(new Item_Space_Decoration(16, 1, 0));
        rv_phones.setAdapter(phoneAdapter);
        vw_phone.findViewById(R.id.btn_cancel).setOnClickListener(v -> phone_dialog.dismiss());
        phone_dialog.show();

        Utils.show_loader(id_progress);
        disposable.add(Retro_Fit.get().get_phone_numbers(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Car>() {
                    @Override
                    public void onSuccess(@NonNull Car c) {
                        Utils.hide_loader(id_progress);
                        if (c.getPhones() != null && c.getPhones().size() > 0) {
                            phones.clear();
                            phones.addAll(c.getPhones());
                            phoneAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Utils.hide_loader(id_progress);
                        Toast.makeText(CarListActivity.this, String.valueOf(e), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    @Override
    public void click_email(Car car) {

    }

    @Override
    public void click_park(Car car, AppCompatImageView imageView, SpinKitView spinKitView) {
        if (Utils.set_parking_icon(CarListActivity.this, car)) {
            if (imageView != null)
                imageView.setVisibility(View.INVISIBLE);
            Utils.show_loader(spinKitView);
            appDB.carDao().delete_car(car);
            new Handler().postDelayed(() -> {
                Utils.hide_loader(spinKitView);
                if (imageView != null) {
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.ic_park_outline);
                }
            }, 1000);
        } else {
            if (imageView != null)
                imageView.setVisibility(View.INVISIBLE);
            Utils.show_loader(spinKitView);
            appDB.carDao().insert_car(car);
            new Handler().postDelayed(() -> {
                Utils.hide_loader(spinKitView);
                if (imageView != null) {
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.ic_park_filled);
                }
            }, 1000);

//            disposable.add(Retro_Fit.get().parking_delete(car.getId())
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeWith(new DisposableSingleObserver<Car>() {
//                        @Override
//                        public void onSuccess(@NonNull Car c) {
//                            Utils.hide_loader(spinKitView);
//                            if (c.getPhones() != null && c.getPhones().size() > 0) {
//                                phones.clear();
//                                phones.addAll(c.getPhones());
//                                phoneAdapter.notifyDataSetChanged();
//                            }
//                        }
//
//                        @Override
//                        public void onError(@NonNull Throwable e) {
//                            Utils.hide_loader(spinKitView);
//                            Toast.makeText(CarListActivity.this, String.valueOf(e), Toast.LENGTH_SHORT).show();
//                        }
//                    }));
        }
    }

    @Override
    public void click_call(String uri) {
        if (!Utils.isEmpty(uri)) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        }
    }

    private void set_sorting_data() {
        choiceModels.add(new ChoiceModel("Standard Sorting", "rel", "")); //sb:rel
        choiceModels.add(new ChoiceModel("Price Ascending", "", ""));
        choiceModels.add(new ChoiceModel("Price Descending", "p", "DOWN")); // sb:p od:DOWN
        choiceModels.add(new ChoiceModel("Km Ascending", "ml", "UP")); //sb:ml od:UP
        choiceModels.add(new ChoiceModel("Km Descending", "ml", "DOWN")); //sb:ml od:DOWN
        choiceModels.add(new ChoiceModel("FR/Construction Year Ascending", "fr", "UP")); //sb:fr od:UP
        choiceModels.add(new ChoiceModel("FR/Construction Year Descending", "fr", "DOWN")); //sb:fr od:DOWN
        choiceModels.add(new ChoiceModel("Oldest Ads First", "doc", "UP")); //sb:doc od:UP
        choiceModels.add(new ChoiceModel("Newest Ads First", "doc", "DOWN")); //sb:doc od:DOWN
    }

    private void open_dialog_sorting() {
        dialog_menu = new AlertDialog.Builder(CarListActivity.this).create();
        final View view = View.inflate(CarListActivity.this, R.layout.dialog_sorting, null);
        dialog_menu.setView(view);
        final AppCompatTextView btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(v -> dialog_menu.dismiss());
        dialog_menu.show();
    }

    private void open_dialog_save_search() {
        dialog_menu = new AlertDialog.Builder(CarListActivity.this).create();
        final View view = View.inflate(CarListActivity.this, R.layout.dialog_save_search, null);
        dialog_menu.setView(view);
        final AppCompatTextView btn_ok = view.findViewById(R.id.btn_ok);
        final AppCompatTextView btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_ok.setOnClickListener(v -> dialog_menu.dismiss());
        btn_cancel.setOnClickListener(v -> dialog_menu.dismiss());
        dialog_menu.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_ads, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (R.id.menu_sort == item.getItemId())
            open_dialog_sorting();
        else if (R.id.menu_save_search == item.getItemId())
            open_dialog_save_search();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposable != null) disposable.dispose();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}