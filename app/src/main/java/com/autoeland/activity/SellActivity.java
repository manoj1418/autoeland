package com.autoeland.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.autoeland.R;
import com.autoeland.databinding.ActivitySellBinding;
import com.autoeland.db.AppDB;
import com.autoeland.db.VersionDB;
import com.autoeland.model.ChoiceModel;
import com.autoeland.model.Make;
import com.autoeland.model.MakeLogos;
import com.autoeland.model.Model;
import com.autoeland.model.Version;
import com.autoeland.utils.Utils;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SellActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    ActivitySellBinding binding;
    List<Make> makes, sel_makes;
    HashMap<String, String> params;
    List<Model> models, sel_models;
    List<Version> versions;
    List<ChoiceModel> conditions, sel_conditions, fuels, sel_fuels, temp_choice;
    Make make;
    Model model;
    List<String> makelist, modelist, temp_model, versionlist;
    ArrayAdapter<String> brand_adapter, model_adapter;
    AppDB appDB;
    VersionDB versDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySellBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.idToolbar.setTitle(getResources().getString(R.string.tx_sell));
        setSupportActionBar(binding.idToolbar);
        binding.idToolbar.setNavigationIcon(R.drawable.ic_back);
        binding.idToolbar.setNavigationOnClickListener(v -> onBackPressed());

        appDB = AppDB.get(this);
        versDB = VersionDB.get(this);

        params = new HashMap<>();
        makes = new ArrayList<>();
        sel_makes = new ArrayList<>();
        models = new ArrayList<>();
        sel_models = new ArrayList<>();
        conditions = new ArrayList<>();
        sel_conditions = new ArrayList<>();
        fuels = new ArrayList<>();
        sel_fuels = new ArrayList<>();
        temp_choice = new ArrayList<>();

        makelist = new ArrayList<>();
        modelist = new ArrayList<>();
        temp_model = new ArrayList<>();
        versions = new ArrayList<>();
        versionlist = new ArrayList<>();

        brand_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, makelist);
        brand_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spBrands.setAdapter(brand_adapter);

        model_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, modelist);
        model_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spModel.setAdapter(model_adapter);

        binding.btnContinue.setOnClickListener(v -> startActivity(new Intent(SellActivity.this, ImageSelectionActivity.class)));
        binding.spBrands.setOnItemSelectedListener(this);
        binding.spModel.setOnItemSelectedListener(this);

        set_data();
    }

    private void set_data() {
        List<Make> temp_make = appDB.makeDao().get_all_make();
        if (temp_make != null) {
            makes.clear();
            makelist.clear();
            makes.addAll(temp_make);
            for (int i = 0; i < makes.size(); i++) {
                makelist.add(makes.get(i).getValue());
            }
            brand_adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.sp_brands) {
            make = makes.get(position);
            models.clear();
            modelist.clear();
            List<Model> ml = new Gson().fromJson(Utils.check_str(load_local("models_for_make_" + make.getKey())), MakeLogos.class).getModels();
            if (ml != null) {
                models.clear();
                models.addAll(ml);
            }
            for (int i = 0; i < models.size(); i++) {
                modelist.add(models.get(i).getValue());
            }
            model_adapter.notifyDataSetChanged();
        } else if (parent.getId() == R.id.sp_model) {
            model = models.get(position);
            versions.clear();
            versionlist.clear();
//            List<Version> temp_version = versDB.versionDao().get_all_version();
//            Log.d("Manoj", "1. " + new Gson().toJson(temp_version));
//            List<Version> temp_version = versDB.versionDao().get_version_from_model(model.getKey(), make.getKey());
//            Log.d("Manoj", "1. " + new Gson().toJson(temp_version));
//            if (temp_version != null) {
//                versions.clear();
//                versions.addAll(temp_version);
//            }
//            for (int i = 0; i < versions.size(); i++) {
//                versionlist.add(versions.get(i));
//            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private String load_local(String fname) {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open(fname);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}