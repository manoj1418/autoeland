package com.autoeland.utils;

import android.util.Base64;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retro_Fit {
    private static class RF {
        static volatile Retrofit retrofit = null;
        static volatile OkHttpClient okHttpClient = null;
        static volatile Api apiService = null;

        static Retrofit get_retrofit() {
            if (okHttpClient == null)
                get_okhttp();
            if (retrofit == null) {
                String BASE_URL = "https://m.mobile.de";
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(okHttpClient)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }

        static void get_okhttp() {
            int time_out = 60;
            String credentials = "fotovoltaicadegandesa@gmail.com" + ":" + "Barcelona77";
            final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//Zm90b3ZvbHRhaWNhZGVnYW5kZXNhQGdtYWlsLmNvbTpCYXJjZWxvbmE3Nw==
            OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(time_out, TimeUnit.SECONDS)
                    .readTimeout(time_out, TimeUnit.SECONDS)
                    .writeTimeout(time_out, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(interceptor);
            httpClient.addInterceptor(chain -> {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Authorization", basic)
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                        .addHeader("User-Agent", "de.mobile.android.app/8.27")
                        .addHeader("x-mobile-api-version", "9")
                        .addHeader("x-mobile-client", "de.mobile.android.app/8.27/b2a22457-fecf-4716-8104-9d12f0df8b35")
                        .addHeader("x-mobile-device-type", "phone")
                        .addHeader("x-mobile-feature-variant", "cd198-vip-leasing-fin:cd198-vip-leasing-fin-test1-android,cd196-srp-popover-financinglink-fin:cd196-srp-popover-financinglink-fin-test2-android,173-adex-endpointAndroid:173-adex-endpointAndroid-with_adex_tracking-android,cd175-adaptive-inline-banner-ad:cd175-adaptive-inline-banner-ad-custom-android")
                        .addHeader("x-mobile-vi", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaWQiOiI1NGVkNzVjMC03MTBmLTRlNGItOGZmNS1hMzdjMWE3Yjg1MzQiLCJkbnQiOnRydWUsImlhdCI6MTYyMjc1MDYxNiwiYXVkIjpbXX0.EQeOBIQ9s1h67kjoGJiyO8NartSVroionSjV5bEBjwE");
                Request request = requestBuilder.build();
                return chain.proceed(request);
            });
            okHttpClient = httpClient.build();
        }

        static Api get_api() {
            if (apiService == null)
                apiService = get_retrofit().create(Api.class);
            return apiService;
        }
    }

    public static Api get() {
        return RF.get_api();
    }
}