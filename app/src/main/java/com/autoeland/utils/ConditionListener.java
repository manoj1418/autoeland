package com.autoeland.utils;

import com.autoeland.model.ChoiceModel;

public interface ConditionListener {
    void select_condition(ChoiceModel cm, boolean is_checked);
}