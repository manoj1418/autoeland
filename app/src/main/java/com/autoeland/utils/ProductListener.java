package com.autoeland.utils;

import com.autoeland.model.Product;

public interface ProductListener {
    void open_product(Product product);
}