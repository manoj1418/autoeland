package com.autoeland.utils;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Item_Space_Decoration extends RecyclerView.ItemDecoration {
    private final int columns;
    private int margin;
    private int orientation;

    public Item_Space_Decoration(@IntRange(from = 0) int margin, @IntRange(from = 0) int columns, @IntRange(from = 0) int orientation) {
        this.margin = margin;
        this.columns = columns;
        this.orientation = orientation;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildLayoutPosition(view);
        if (orientation == 0) {
            outRect.right = margin;
            outRect.bottom = margin;
            if (position < columns) {
                outRect.top = margin;
            }
            if (position % columns == 0) {
                outRect.left = margin;
            }
        } else if (orientation == 1) {
            outRect.top = margin;
            outRect.bottom = margin;
            if (position % columns == 0) {
                outRect.left = margin * 2;
            }
            if (position % columns == columns - 1) {
                outRect.right = margin * 2;
            } else
                outRect.right = margin;
        }
    }
}