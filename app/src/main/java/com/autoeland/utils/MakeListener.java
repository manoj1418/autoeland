package com.autoeland.utils;

import com.autoeland.model.Make;

public interface MakeListener {
    void click_make(Make mk);

    void delete_make(Make mk);
}