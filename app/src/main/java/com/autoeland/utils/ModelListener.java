package com.autoeland.utils;

import com.autoeland.model.Model;

public interface ModelListener {
    void insert_model(Model m);

    void delete_model(Model m);
}