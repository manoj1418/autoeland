package com.autoeland.utils;

import com.autoeland.model.ChoiceModel;

public interface FuelListener {
    void select_fuel(ChoiceModel cm, boolean is_checked);
}