package com.autoeland.utils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retro_Fit_3 {
    private static class RF {
        static volatile Retrofit retrofit = null;
        static volatile OkHttpClient okHttpClient = null;
        static volatile Api apiService = null;

        static Retrofit get_retrofit() {
            if (okHttpClient == null)
                get_okhttp();
            if (retrofit == null) {
                String BASE_URL = "https://pro2.wallapop.com";
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(okHttpClient)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }

        static void get_okhttp() {
            int time_out = 60;
            OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(time_out, TimeUnit.SECONDS)
                    .readTimeout(time_out, TimeUnit.SECONDS)
                    .writeTimeout(time_out, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(interceptor);
            httpClient.addInterceptor(chain -> {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json");
                Request request = requestBuilder.build();
                return chain.proceed(request);
            });
            okHttpClient = httpClient.build();
        }

        static Api get_api() {
            if (apiService == null)
                apiService = get_retrofit().create(Api.class);
            return apiService;
        }
    }

    public static Api get() {
        return RF.get_api();
    }
}