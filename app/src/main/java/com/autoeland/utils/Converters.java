package com.autoeland.utils;

import androidx.room.TypeConverter;

import com.autoeland.model.CarImage;
import com.autoeland.model.Contact;
import com.autoeland.model.Financing;
import com.autoeland.model.Parking;
import com.autoeland.model.Phone;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class Converters {
    @TypeConverter
    public String get_string(List<String> strings) {
        if (strings == null) return null;
        return new Gson().toJson(strings, new TypeToken<List<String>>() {
        }.getType());
    }

    @TypeConverter
    public List<String> set_string(String s) {
        if (s == null) return null;
        return new Gson().fromJson(s, new TypeToken<List<String>>() {
        }.getType());
    }

    @TypeConverter
    public String get_car_image(List<CarImage> carImages) {
        if (carImages == null) return null;
        return new Gson().toJson(carImages, new TypeToken<List<CarImage>>() {
        }.getType());
    }

    @TypeConverter
    public List<CarImage> set_car_image(String s_car_images) {
        if (s_car_images == null) return null;
        return new Gson().fromJson(s_car_images, new TypeToken<List<CarImage>>() {
        }.getType());
    }

    @TypeConverter
    public String get_contact(Contact contact) {
        if (contact == null) return null;
        return new Gson().toJson(contact, new TypeToken<Contact>() {
        }.getType());
    }

    @TypeConverter
    public Contact set_contact(String s_contact) {
        if (s_contact == null) return null;
        return new Gson().fromJson(s_contact, new TypeToken<Contact>() {
        }.getType());
    }

    @TypeConverter
    public String get_phone(List<Phone> phones) {
        if (phones == null) return null;
        return new Gson().toJson(phones, new TypeToken<List<Phone>>() {
        }.getType());
    }

    @TypeConverter
    public List<Phone> set_phone(String s_phones) {
        if (s_phones == null) return null;
        return new Gson().fromJson(s_phones, new TypeToken<List<Phone>>() {
        }.getType());
    }

    @TypeConverter
    public String get_finance(List<Financing> financings) {
        if (financings == null) return null;
        return new Gson().toJson(financings, new TypeToken<List<Financing>>() {
        }.getType());
    }

    @TypeConverter
    public List<Financing> set_finance(String s_financings) {
        if (s_financings == null) return null;
        return new Gson().fromJson(s_financings, new TypeToken<List<Financing>>() {
        }.getType());
    }

    @TypeConverter
    public String get_parking(Parking parking) {
        if (parking == null) return null;
        return new Gson().toJson(parking, new TypeToken<Parking>() {
        }.getType());
    }

    @TypeConverter
    public Parking set_parking(String s_parking) {
        if (s_parking == null) return null;
        return new Gson().fromJson(s_parking, new TypeToken<Parking>() {
        }.getType());
    }
}