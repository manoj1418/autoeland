package com.autoeland.utils;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;

import com.autoeland.R;
import com.autoeland.db.AppDB;
import com.autoeland.model.Car;
import com.autoeland.model.ChoiceModel;
import com.autoeland.model.Model;
import com.github.ybq.android.spinkit.SpinKitView;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utils {

    public static String get_user(Context ctx) {
        return "1";
    }

    public static boolean isEmpty(AppCompatEditText editText) {
        return check_str(String.valueOf(editText.getText())).length() == 0;
    }

    public static boolean isEmpty(String value) {
        return check_str(value).length() == 0;
    }

    public static String check_str(String value) {
        return value != null ? value.trim() : "";
    }

    public static int str_int(String value) {
        return !isEmpty(value) ? Integer.parseInt(value.trim()) : 0;
    }

    public static void show_loader(SpinKitView spinKitView) {
        if (spinKitView != null)
            spinKitView.setVisibility(View.VISIBLE);
    }

    public static void hide_loader(SpinKitView spinKitView) {
        if (spinKitView != null)
            spinKitView.setVisibility(View.GONE);
    }

    public static String get_currency_format(String amount) {
        if (amount != null && !amount.equals(""))
            return new DecimalFormat("#,###,##0").format(Double.parseDouble(amount.replace(",", "")));
        return "0.00";
    }

    public static String get_currency_symbol(String currency, String s) {
        String sym = "";
        if (currency != null) {
            if (currency.equals("USD")) sym = "$";
            if (currency.equals("EUR")) sym = "€";
        }
        if (s != null) {
            return sym + s;
        }
        return "";
    }

    public static String set_image_url(String url) {
        return isEmpty(url) ? "" : "https://" + url + "_1.jpg";
    }

    public static void set_image(String url, ImageView imageView) {
        if (isEmpty(url)) {
            imageView.setImageResource(R.drawable.ic_placeholder);
            return;
        }
        imageView.setImageDrawable(null);
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .priority(Picasso.Priority.HIGH)
                .into(imageView);
    }

    public static void set_image(String url, AppCompatImageView imageView) {
        if (isEmpty(url)) {
            imageView.setImageResource(R.drawable.ic_placeholder);
            return;
        }
        imageView.setImageDrawable(null);
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .priority(Picasso.Priority.HIGH)
                .into(imageView);
    }

    public static String get_desc(List<String> details) {
        if (details == null) return "";
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : details) {
            stringBuilder.append(s.replace(", ", "").replace(",", "").trim()).append(" . ");
        }
        return String.valueOf(stringBuilder);
    }

    public static String combine_str(String a, String b) {
        return check_str(a) + " " + check_str(b);
    }

    public static String get_make_model(String make, String model) {
        return check_str(make) + ";" + check_str(model);
    }

    public static String get_models(List<Model> models) {
        StringBuilder stringBuilder = new StringBuilder();
        if (models != null) {
            for (Model m : models) {
                if (stringBuilder.length() != 0)
                    stringBuilder.append(", ");
                stringBuilder.append(m.getValue());
            }
        }
        return String.valueOf(stringBuilder);
    }

    public static String get_choice(List<ChoiceModel> choiceModels) {
        StringBuilder stringBuilder = new StringBuilder();
        if (choiceModels != null) {
            for (ChoiceModel c : choiceModels) {
                if (stringBuilder.length() != 0)
                    stringBuilder.append(", ");
                stringBuilder.append(c.getName());
            }
        }
        return String.valueOf(stringBuilder);
    }

    public static boolean set_parking_icon(Car car) {
        boolean result = false;
        if (car.getId() != 0 && car.getParking() != null && car.getParking().getAdId() != 0)
            result = car.getId() == car.getParking().getAdId();
        return result;
    }

    public static boolean set_parking_icon(Context ctx, Car car) {
        boolean result = false;
        if (car.getId() != 0)
            result = AppDB.get(ctx).carDao().is_car_available(car.getId()) != 0;
        return result;
    }

    public static String get_time(Date date) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa", Locale.US);
        return simpleDateFormat.format(date);
    }

    public static String get_price_desc(String from, String to) {
        String desc = "";
        if (!isEmpty(from))
            desc = "from €" + from + " ";
        if (!isEmpty(to))
            desc += "to €" + to;
        return desc;
    }

    public static String get_first_reg_desc(String from, String to) {
        String desc = "";
        if (!isEmpty(from))
            desc = "from " + from + " ";
        if (!isEmpty(to))
            desc += "to " + to;
        return desc;
    }

    public static String get_mileage_desc(String from, String to) {
        String desc = "";
        if (!isEmpty(from))
            desc = "from " + from + " km ";
        if (!isEmpty(to))
            desc += "to " + to + " km";
        return desc;
    }

    public static String get_year(int year) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, year);
        return String.valueOf(cal.get(Calendar.YEAR));
    }
}