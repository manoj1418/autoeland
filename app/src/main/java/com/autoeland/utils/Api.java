package com.autoeland.utils;

import com.autoeland.model.AdsDetails;
import com.autoeland.model.Car;
import com.autoeland.model.CarList;
import com.autoeland.model.Parking;
import com.autoeland.model.ProductList;
import com.autoeland.model.ResponseModel;

import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface Api {
    @GET("/svc/s")
    Single<CarList> get_data(@QueryMap Map<String, String> params);

    @GET("/svc/s")
    Single<CarList> get_search_count(@QueryMap Map<String, String> params);
//    Single<SearchCount> get_search_count(@Query("vc") String vc, @Query("ms") String ms, @Query("ps") Integer ps, @Query("psz") Integer psz, @Query("p") String p, @Query("con") String con, @Query("dam") Integer dam);

    @GET("/svc/s")
    Single<CarList> get_car_filtered(@Query("vc") String vc, @Query("ms") String ms, @Query("ps") String ps, @Query("psz") String psz, @Query("con") String con, @Query("p") String p, @Query("dam") String dam);

    @GET("/svc/call-tracker-number/{id}")
    Single<Car> get_phone_numbers(@Path("id") int id);

    @GET("/svc/a/{id}")
    Single<AdsDetails> get_ads_details(@Path("id") int id, @Query("_no-call-tracker") boolean no_call_tracker, @Query("searchId") String search_id);

    @DELETE("/svc/my/parkings/")
    Single<Parking> parking_delete(@Query("id") int id);

    @POST("/svc/my/jwt")
    Single<ResponseModel> login();

    @Multipart
    @POST("svc/my/adimages")
    Single<ResponseModel> upload_images(@Part MultipartBody.Part img);

    @GET("/api/v3/general/wall")
    Single<ProductList> get_product_list(@Query("category_ids") int category_ids, @Query("density_type") int density_type, @Query("latitude") double latitude, @Query("longitude") double longitude, @Query("start") int start, @Query("step") int step, @Query("num_results") int num_results, @Query("search_id") String search_id, @Query("filters_source") String category_slider);
}