package com.autoeland.utils;

import androidx.appcompat.widget.AppCompatImageView;

import com.autoeland.model.Car;
import com.autoeland.model.Phone;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

public interface CarListener {
    void click_car(Car car);

    void click_call(int id);

    void click_email(Car car);

    void click_park(Car car, AppCompatImageView imageView, SpinKitView spinKitView);
}